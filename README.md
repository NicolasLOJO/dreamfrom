# DREAMFROM

Site web pour les projets qui ne peuvent aboutir seul

## Avant de commencer

C'est instruction ont pour but de vous guider tout au long de l'installation

## Prérequis

Il vous faudra : 
* une gestionnaire de base de donnée (type mysql)
* un host virtuel qui vous servira de racine a votre projet web
* un éditeur de texte pour faire la configuration ou un IDE pour modifier le code

## Installation

Télécharger ou cloner le projet et le mettre a la racine de votre serveur virtuelle préalablement crée. (*Si le projet a était télécharger pensez a le décompresser*)

Importer la base de donnée qui est dans le dossier 'database' du projet.

Configurer le fichier database.json qui est dans le dossier 'config' du projet.

``` json
{
    "driver" : "mysql",
    "host" : "localhost",
    "port" : "3306",
    "dbname" : "DreamFrom",
    "username" : "",
    "password" : ""
}
```

Si le fichier .htaccess n'est pas présent a la racine du projet crée en un et y copier ce code

```
DirectoryIndex index.php index.html
Options +FollowSymlinks
RewriteEngine on

#redirect all api calls to api/index.php
RewriteRule ^api/((?!index\.php$).+)$ api/index.php [L,NC]

#if the request is a file, folder or symlink that exists, serve it up
RewriteCond %{REQUEST_FILENAME} -f [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^ - [L]

#otherwise, serve your index.html app
RewriteRule ^(.+)$ index.php [L]
```

## Bon à savoir

Le projet possède 2 compte par defaut.

1. Administrateur
    * Nom d'utilisateur: root.
    * Mot de passe: root

2. Test
    * Nom d'utilisateur: test.
    * Mot de passe: test.

## Crée avec
* Le framework Beweb avec quelque petite modification
* VisualCode pour le coté code

## Autheur
* Nicolas - *Chef de projet*
* Yannis - *Le bon bosseur*
* Judicaël - *Le pro Netflix*