<?php

class BarController extends Controller {

    public function index(){
        $cat = new DAOCategorie();
        $subCat = new DAOSouscategories();
        $bar = new DAOBar();
        $data = array("cat" => $cat->getAll(), "sub" => $subCat->getAll(), "bar" => $bar->getAll(), 'event' => EventController::getEvent());
        $this->render("bar/home_bar", $data);
    }

    public function addBar($cat, $status = null){
        $id = explode(",", $cat);
        $categorie = new Categorie();
        $cat = $categorie->set_id($id[0])->load();
        $sub = new Souscategories();
        $subcat = $sub->set_id($id[1])->load();
        if($status){
            $data = array("status" => $status, "cat" => $cat, "sub" => $subcat, "event" => EventController::getEvent());
        } else {
            $data = array("cat" => $cat, "sub" => $subcat, "event" => EventController::getEvent());
        }
        $this->render("bar/home_bar_add", $data);
    }

    public function createBar($cat){
        $post = $this->inputPost();
        $bar = new Bar();
        if(!empty($post['bar_title']) && !empty($post['content'])){
            $bar->set_titre($post['bar_title'])->set_content($post['content'])->set_id_categorie($post['cat'])->set_id_souscategorie($post['sub'])->set_id_user($_SESSION['user_id'])->update();
            header("Location: /Bar");
        } else {
            $status = "Veuillez remplir tout les champs";
            $this->addBar($cat, $status);
        }
    }

    public function getBar($id){
        $bar = new DAOBar();
        $bar = $bar->customQuery("SELECT bar.*,user.id as id_author, user.username, user.avatar, user.contact FROM bar INNER JOIN user ON user.id = bar.id_user WHERE bar.id = $id");
        $com = new DAOCommentaires();
        $com = $com->customQuery("SELECT commentaires.*, user.id as id_author, user.username, user.avatar, user.contact FROM commentaires INNER JOIN user ON user.id = commentaires.id_user WHERE commentaires.id_bar = $id ORDER BY commentaires.created_date DESC");
        $data = array("bar" => $bar, "event" => EventController::getEvent(), "com" => $com);
        $this->render("bar/home_bar_one", $data);
    }

    public function commentBar($id, $status = null){
        $bar = new DAOBar();
        $bar = $bar->customQuery("SELECT bar.*,user.id as id_author, user.username, user.avatar, user.contact FROM bar INNER JOIN user ON user.id = bar.id_user WHERE bar.id = $id");
        if($status){
            $data = array("bar" => $bar, "event" => EventController::getEvent(), 'status' => $status);
        } else {
            $data = array("bar" => $bar, "event" => EventController::getEvent());
        }
        $this->render("bar/home_comment_bar", $data);
    }

    public function createComment($id){
        $post = $this->inputPost();
        if(!empty($post['answer'])){
            $com = new Commentaires();
            $com->set_content($post['answer'])->set_id_user($_SESSION['user_id'])->set_id_bar($id)->update();
            header("Location: /Bar/$id");
        } else {
            $this->commentBar($id, "Veuillez remplir le message");
        }
    }
}