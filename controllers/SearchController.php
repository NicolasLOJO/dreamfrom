<?php

class SearchController extends Controller {

    public function search(){
        $post = $this->inputPost();
        $project = new DAOProject();
        $value = array("hide" => "1", "search" => "%".$post['search']."%");
        $project = $project->customData("SELECT project.id as id_project, user.username as author, project.titre, project.content, project.accepted, Null as username, Null as hide,Null as avatar, project.created_date FROM project LEFT JOIN user ON user.id = project.id_user WHERE project.accepted = 1 AND project.titre LIKE :search OR project.content LIKE :search OR user.username LIKE :search UNION ALL SELECT Null as col1, username as author, Null as col3, Null as col4, Null as col5, username, private,avatar, created_date FROM user WHERE NOT private = :hide AND username LIKE :search ORDER BY created_date DESC", $value);
        $data = array("result" => $project, "event" => EventController::getEvent());
        $this->render("search/search_home", $data);
    }

}