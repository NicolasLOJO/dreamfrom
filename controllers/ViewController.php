<?php

class ViewController extends Controller{


    public function getHome(){
        $event = EventController::getEvent();
        if(isset($_SESSION['user_id'])){
            $notif = new DAONotif();
            if($_SESSION['status'] == 'admin'){
                $notif = $notif->customData("SELECT notif.*, user.username, project.id as num_project FROM notif INNER JOIN user ON notif.expediteur = user.id INNER JOIN project ON notif.id_project = project.id WHERE notif.id_user = ".$_SESSION['user_id']." OR notif.status = '".$_SESSION['status']."'");
            } else {
                $notif = $notif->customData("SELECT notif.*, user.username, project.id as num_project FROM notif INNER JOIN user ON notif.expediteur = user.id INNER JOIN project ON notif.id_project = project.id WHERE notif.id_user = ".$_SESSION['user_id']);
            }
            $user = new User();
            $user = $user->set_id($_SESSION['user_id'])->load();
            $project = new DAOProject();
            $project = $project->getAllBy("id_user = ".$_SESSION['user_id']);
            $collab = new DAOCollab();
            $collab = $collab->customData("SELECT project.* FROM collab INNER JOIN project ON collab.id_project = project.id WHERE collab.id_user = ".$user->get_id());
            $data = array("load" => "./views/modules/myproject.php", 'notif' => $notif, "projet" => $project, "collab" => $collab, 'event' => $event);
            $this->render("default/home", $data); 
        } else {
            $data = array('event' => $event);
            $this->render("default/home", $data);
        }
    }

    public function getDoc(){
        $this->render("default/doc");
    }

    public function adminDash(){
        if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin'){
            $user = new DAOUser();
            $user = $user->customData("SELECT COUNT(*) AS count FROM user");
            $project = new DAOProject();
            $project = $project->customData("SELECT COUNT(*) AS count FROM project");
            $data = array("project" => $project, "user" => $user);
            $this->render("admin/admin_dash", $data);
        } else {
            $this->render('default/error');
        }
    }
}