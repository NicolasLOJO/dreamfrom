<?php

class AdminController extends Controller {

    public function getMember(){
        if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin'){
            $user = new DAOUser();
            $user = $user->getAll();
            $data = array('user' => $user);
            $this->render('admin/admin_member_home', $data);
        } else {
            $this->render('default/error');
        }
    }

    public function getProject(){
        if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin'){
            $project = new DAOProject();
            $project = $project->customData("SELECT project.*, user.username FROM project LEFT JOIN user ON project.id_user = user.id");
            $data = array('project' => $project);
            $this->render('admin/admin_project_home', $data);
        } else {
            $this->render("default/error");
        }
        
    }

    public function deleteMember($id){
        if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin'){
            $collab = new DAOCollab();
            $key = new DAOProject();
            $key->disableKey();
            foreach($collab->getAll() as $value){
                if($value['id_user'] == $id){
                    $collabdel = new Collab();
                    $collabdel->set_id($value['id'])->remove();
                }
            }
            $project = new DAOProject();
            foreach($project->getAll() as $value){
                if($value['id_user'] == $id){
                    $project = new Project();
                    $project->set_id($value['id'])->set_id_user("0")->update();
                }
            }
            $user = new User();
            $user->set_id($id)->remove();
            $key->enableKey();
            header("Location: /admin/membres");
        } else {
            $this->render("default/error");
        }
    }

    public function updateMember(){
        if(isset($_SESSION['status']) && $_SESSION['status'] == "admin"){
            $post = $this->inputPost();
            var_dump($post);
            $user = new User();
            $user->set_id($post['id'])->set_username($post['pseudo'])->set_nom($post['nom'])->set_prenom($post['prenom'])->set_status($post['status'])->update();
            header("Location: /admin/membres");
        } else {
            $this->render("default/error");
        }
    }

    public function getSetting(){
        if(isset($_SESSION['status']) && $_SESSION['status'] == "admin"){
            $cat = new DAOCategorie();
            $data = array('subcat' => $cat->getAll());
            $this->render('admin/admin_setting', $data);
        } else {
            $this->render("default/error");
        }
    }

    public function addCat(){
        if(isset($_SESSION['status']) && $_SESSION['status'] == "admin"){
            $post = $this->inputPost();
            if(!empty($post['cat']) && !is_null($post['cat'])){
                $cat = new Categorie();
                $name = $post['cat'];
                $cat->set_name($name)->update();
            }
            if(!empty($post['subcat']) && $post['id_cat'] != "0"){
                $sub = new Souscategories();
                $sub->set_name($post['subcat'])->set_id_categories($post['id_cat'])->update();
            }
            header("Location: /admin/setting");
        } else {
            $this->render("default/error");
        }
    }

    public function deleteProject($id){
        if(isset($_SESSION['status']) && $_SESSION['status'] == "admin"){
            $key = new DAOProject();
            $key->disableKey();
            $collab = new DAOCollab();
            $collab = $collab->getAllBy("id_project = $id");
            foreach($collab as $value){
                $delCol = new Collab();
                $delCol->set_id($value['id'])->remove();
            }
            $notif = new DAONotif();
            $notif = $notif->getAllBy("id_project = $id");
            foreach($notif as $value){
                $delNot = new Notif();
                $delNot->set_id($value['id'])->remove(); 
            }
            $project = new Project();
            $project->set_id($id)->remove();
            $key->enableKey();
            header("Location: /admin/projects");
        } else {
            $this->render("default/error");
        }
    }
}