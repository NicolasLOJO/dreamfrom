<?php

class AjaxController extends Controller {

    public function getMember(){
        if(isset($_SERVER['HTTP_REFERER'])){
            $user = new DAOUser();
            $user = $user->getAll();
            header('Content-Type: application/json');
            echo json_encode($user);
        } else {
            $this->render("default/error");
        }
    }

    public function getCollab($id){
        if(isset($_SERVER['HTTP_REFERER'])){
            $user = new DAOCollab();
            $user = $user->customData("SELECT collab.*, user.username, notif.accepted, notif.id_project FROM collab RIGHT JOIN user ON collab.id_user = user.id RIGHT JOIN notif ON notif.id_user = user.id WHERE collab.id_project = $id OR notif.id_project = $id AND user.username IS NOT NULL");
            header('Content-Type: application/json');
            echo json_encode($user);
        } else {
            $this->render("default/error");
        }
    }

    public function getDepartement(){
        if(isset($_SERVER['HTTP_REFERER'])){
            $dep = new DAODepartement();
            $dep = $dep->getAll();
            header('Content-Type: application/json');
            echo json_encode($dep);
        } else {
            $this->render("default/error");
        }
    }

    public function getNotif(){
        $id = $_SESSION['user_id'];
        $status = $_SESSION['status'];
        $notif = new DAONotif();
        if($status == "member"){
            $notif = $notif->customData("SELECT notif.*, project.titre, user.username FROM notif INNER JOIN project ON notif.id_project = project.id INNER JOIN user ON notif.expediteur = user.id WHERE notif.id_user = $id AND notif.accepted IS NULL");
        } else {
            $notif = $notif->customData("SELECT notif.*, project.titre, user.username FROM notif INNER JOIN project ON notif.id_project = project.id INNER JOIN user ON notif.expediteur = user.id WHERE notif.id_user = $id AND notif.accepted IS NULL OR notif.status = '$status' AND notif.accepted IS NULL");
        }
        header('Content-Type: application/json');
        echo json_encode($notif);
    }

    public function getCat(){
        $post = $this->inputPost();
        $cat = new DAOSouscategories();
        $cat = $cat->getAllBy("id_categories = ".$post['id']);
        header('Content-Type: application/json');
        echo json_encode($cat);
    }
}