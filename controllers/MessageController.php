<?php

class MessageController extends Controller {

    public function getContact(){
        $this->render("message/contact_home");
    }

    public function getMessage($id = null){
        if(isset($_SESSION['status'])){
            $msg = new DAOPrivatemsg();
            $select = array("privatemsg.*", "user.username");
            $join = array("user" => "privatemsg.id_user = user.id");
            $order = array("readed_message" => "ASC", "created_date" => "DESC");
            if($id){
                $msg = $msg->getAllBy(array("destinataire = ".$_SESSION['user_id'], "id_Project = $id"),$select, $join, $order);
                $this->readedMessage($msg);
                $data = array('message' => $msg, "load" => "./views/modules/project_msg.php", 'event' => EventController::getEvent());
            } else {
                $msg = $msg->getAllBy("destinataire = ".$_SESSION['user_id'],$select, $join, $order);
                $room = $this->getProjectMsg($msg);
                $data = array("message" => $msg, "load" => "./views/modules/private.php", "thread" => $room, 'event' => EventController::getEvent());
            }
            $this->render("default/home", $data);
        } else {
            $this->render("default/error");
        }
    }

    public function viewMessage($id){
        if(isset($_SESSION['status'])){
            $msg = new Privatemsg();
            $msg->set_id($id)->set_readed_message("1");
            $msg->update();
            $msg = $msg->load();
            $user = new User();
            $user->set_id($msg->get_id_user());
            $user = $user->load();
            $data = array('load' => './views/modules/seemsg.php', 'message' => $msg, 'user' => $user, 'event' => EventController::getEvent());
            $this->render('default/home', $data);
        } else {
            $this->render("default/error");
        }
    }

    public function answerMessage($id){
        if(isset($_SESSION['status'])){
            $post = $this->inputPost();
            $user = new DAOUser();
            $msg = new Privatemsg();
            $msg->set_destinataire($post['dest'])->set_id_project($id)->set_content($post['answer'])->set_object($post['object'])->set_id_user($_SESSION['user_id']);
            $msg->update();
            header("Location: /messages");
        } else {
            $this->render("default/error");
        }
    }

    public function createMessage(){
        if(isset($_SESSION['status'])){
            $post = $this->inputPost();
            $user = new DAOUser();
            $msg = new Privatemsg();
            $msg->set_destinataire($post['dest'])->set_id_project($post['pro'])->set_content($post['content'])->set_object($post['object'])->set_id_user($_SESSION['user_id']);
            $msg->update();
            header("Location: /messages");
        } else {
            $this->render("default/error");
        }
    }

    public function sendedMessage(){
        if(isset($_SESSION['status'])){
            $message = new DAOPrivatemsg();
            $select = array("privatemsg.*", "user.username");
            $join = array("user" => "privatemsg.destinataire = user.id");
            $order = array("created_date" => "DESC");
            $data = array("message" => $message->getAllBy("id_user = ".$_SESSION['user_id'],$select, $join, $order), "load" => "./views/modules/seesentmsg.php", 'event' => EventController::getEvent());
            $this->render("default/home", $data);
        } else {
            $this->render("default/error");
        }
    }

    private function getProjectMsg($msg){
        if(isset($_SESSION['status'])){
            $room = [];
            foreach($msg as $thread){
                $id = $thread['id_project'];
                $project = new Project();
                $project = $project->set_id($id)->load();
                if($project == false){
                    $status = "Projet supprimé";
                } else {
                    $status = $project->get_titre();
                }
                $array = array("id_project" => $id, "newmsg" => $this->countNewMsg($id) , "project" => $status, "oldmsg" => $this->countOldMsg($id));
                if($this->searchArrayKeyVal("id_project", $id, $room) == false){
                    $room[$id] = $array;
                }
            }
            return $room;
        } else {
            $this->render("default/error");
        }  
    }

    private function countOldMsg($id){
        $message = new DAOPrivatemsg();
        $oldmsg = $message->customData("SELECT project.titre, COUNT('*') FROM privatemsg INNER JOIN project ON privatemsg.id_project = project.id WHERE destinataire = ".$_SESSION['user_id']." AND id_project = $id");
        return $oldmsg["COUNT('*')"];
    }

    private function countNewMsg($id){
        $message = new DAOPrivatemsg();
        $newmsg = $message->customData("SELECT project.titre, COUNT('*') FROM privatemsg INNER JOIN project ON privatemsg.id_project = project.id WHERE destinataire = ".$_SESSION['user_id']." AND id_project = $id AND readed_message = 0");
        return $newmsg["COUNT('*')"];
    }

    private function readedMessage($msg){
        foreach($msg as $value){
            if($value['readed_message'] == 0){
                $pm = new Privatemsg();
                $pm->set_id($value['id'])->set_readed_message(1)->update();
            }
        }
    }

    public function formMessage(){
        if(isset($_SESSION['status'])){
            $load = "./views/modules/form_message.php";
            $project = new DAOProject();
            $project = $project->getAllBy("id_user = ".$_SESSION['user_id']);
            $list_collab = [];
            $collab = new DAOCollab();
            foreach($project as $col){
                $result = $collab->customData("SELECT user.username, user.id AS id_dest FROM collab INNER JOIN user ON collab.id_user = user.id WHERE collab.id_project = ".$col['id']);
                $list_collab[$col['id']] = $result;
            }
            $data = array("load" => $load, 'pro' => $project, 'user' => $list_collab, 'event' => EventController::getEvent());
            $this->render("default/home", $data);
        } else {
            $this->render("default/error");
        }
    }

    private function searchArrayKeyVal($sKey, $id, $array) {
        foreach ($array as $key => $val) {
            if ($val[$sKey] == $id) {
                return $key;
            }
        }
        return false;
    }

    public function newMessage($id){
        $project = new DAOProject();
        $project = $project->customData("SELECT * FROM project WHERE id_user = $id OR id_user = ".$_SESSION['user_id']);
        $user = new User();
        $user = $user->set_id($id)->load();
        if($user->get_contact() == 0){
            $data = array("pro" => $project, "event" => EventController::getEvent(), "user" => $user);
            $this->render("message/new_home_message", $data);
        } else {
            $this->render("default/error");
        }
    }

    public function sendMessage($id){
        $post = $this->inputPost();
        $project = new Project();
        $project = $project->set_id($post['object'])->load();
        $message = new Privatemsg();
        if(!empty($post['dest']) && $post['object'] != 0 && !empty($post['msg'])){
            $message->set_object($project->get_titre())->set_content($post['msg'])->set_id_user($_SESSION['user_id'])->set_destinataire($id)->set_id_project($post['object'])->update();
        }
        header("Location: /member");
    }
}