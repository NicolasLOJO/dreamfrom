<?php

class ProjectController extends Controller {
    
    public function index(){
        $cat = new DAOCategorie();
        $subCat = new DAOSouscategories();
        $project = new DAOProject();
        $data = array("cat" => $cat->getAll(), "sub" => $subCat->getAll(), "projet" => $project->getAll(), 'event' => EventController::getEvent());
        $this->render("project/home_project", $data);
    }

    public function getProject($id){
        if(isset($_SESSION['status'])){
            $project = new Project();
            $project->set_id($id);
            $project = $project->load();
            $user = new User();
            if($project->get_id_user() == 0){
                $user = $user->set_username("En attente de reprise")->set_avatar("default.png");
                $contact = true;
            } else {
                $user->set_id($project->get_id_user());
                $user = $user->load();
                $contact = false;
            }
            $task = new DAOTask();
            $join = array("user" => "task.id_user = user.id");
            $select = array("task.*, user.username, user.avatar");
            $task = $task->getAllBy("id_project = $id", $select, $join);
            $collab = new DAOCollab();
            $join = array("user" => "collab.id_user = user.id");
            $notif = new DAONotif();
                $notif = $notif->customData("SELECT notif.accepted, user.username, user.avatar FROM notif INNER JOIN user ON user.id = notif.id_user WHERE id_project = $id AND id_user IS NOT NULL");
            $data = array("projet" => $project, "notif" => $notif, "user" => $collab->getAllBy("id_project = $id",false , $join), "boss" => $user, "contact" => $contact, 'event' => EventController::getEvent(), 'task' => $task);
            $this->render("project/home_one_project", $data);
        } else {
            $data = array("status" => "Vous devez être connecté pour voir les projets");
            $this->render('user/login_home', $data);
        }
    }

    public function createProject(){
        if(isset($_SESSION['status'])){
            $cat = new DAOCategorie();
            $subCat = new DAOSouscategories();
            $data = array("load" => "./views/modules/create_project.php", "cat" => $cat->getAll(), "subCat" => $subCat->getAll(),'event' => EventController::getEvent());
            $this->render("default/home", $data);
        } else {
            $this->render("default/error");
        }
    }

    public function addProject(){
        if(isset($_SESSION['status'])){
            $post = $this->inputPost();
            $projet = new Project();
            $projet = $projet->set_titre($post['ptitle'])->set_content($post['pdesc'])->set_id_categorie($post['pcat'])->set_id_souscategorie($post['psubcat'])->set_id_user($_SESSION['user_id'])->update();
            $notif = new Notif();
            $notif->set_status('admin')->set_id_project($projet->get_id())->set_expediteur($_SESSION['user_id'])->set_lien('/Projects/')->set_type('project')->update();
            header("Location: /Projects");
        } else {
            $this->render("default/error");
        }
    }

    public function acceptProject($id){
        if(isset($_SESSION['status'])){
            $notif = new Notif();
            $notif->set_id($id);
            $verif = $notif->load();
            if(!empty($verif) && $verif->get_hidden != true){
                $notif->set_accepted(1)->set_hidden(1)->update();
                $projet = new Project();
                $projet->set_id($verif->get_id_project())->set_accepted(1)->update();
                header("Location: /");
            } else {
                echo "Demande non existante";
            }
        } else {
            $this->render("default/error");
        }
    }

    public function declineProject($id){
        if(isset($_SESSION['status'])){
            $notif = new Notif();
            $notif->set_id($id);
            $verif = $notif->load();
            if(!empty($verif) && $verif->get_hidden != true){
                $notif->set_accepted(1)->set_hidden(1)->update();
                $projet = new Project();
                $projet->set_id($verif->get_id_project())->set_accepted(0)->update();
                header("Location: /");
            } else {
                echo "Demande non existante";
            }
        } else {
            $this->render("default/error");
        }
    }

    public function editProject($id){
        if(isset($_SESSION['status'])){
            $project = new Project();
            $project->set_id($id);
            $project = $project->load();
            if($project->get_id_user() == $_SESSION['user_id'] || $_SESSION['status'] == 'admin'){
                $user = new User();
                if($project->get_id_user() == 0){
                    $user = $user->set_username('En attente de reprise');
                } else {
                    $user->set_id($project->get_id_user());
                    $user = $user->load();
                }
                $task = new DAOTask();
                $join = array("user" => "task.id_user = user.id");
                $select = array("task.*, user.username, user.avatar");
                $task = $task->getAllBy("id_project = $id", $select, $join);
                $collab = new DAOCollab();
                $join = array("user" => "collab.id_user = user.id");
                $notif = new DAONotif();
                $notif = $notif->customData("SELECT notif.accepted, user.username, user.avatar FROM notif INNER JOIN user ON user.id = notif.id_user WHERE id_project = $id AND id_user IS NOT NULL");
                $data = array('load' => './views/modules/edit_project.php',"task" => $task, "notif" => $notif, "projet" => $project, "user" => $collab->getAllBy("id_project = $id", false,  $join), "boss" => $user, 'event' => EventController::getEvent());
                $this->render("default/home", $data);
            }
        } else {
            $this->render("default/error");
        }
    }

    public function updateProject($id){
        if(isset($_SESSION['status'])){
            $project = new Project();
            $post = $this->inputPost();
            $project->set_id($id);
            if(!empty($post['desc'])){
                $project->set_content($post['desc']);
            }
            if(!empty($post['title'])){
                $project->set_titre($post['title']);
            }
            $project->update();
            $data = array("status" => "ok", "response" => 200);
            echo json_encode($data);
        }
    }

    public function deleteProject($id){

    }

    public function quitProject($id){
        if(isset($_SESSION['status'])){
            $project = new Project();
            $project->set_id($id);
            $verif = $project->load();
            $collab = new DAOCollab();
            $collab = $collab->getAllBy(array("id_user = ".$_SESSION['user_id'], "id_project = ".$id));
            if($verif->get_id_user() == $_SESSION['user_id']){
                $project = new DAOProject();
                $project->disableKey();
                $project->customExec("UPDATE project SET id_user = 0 WHERE id = $id");
                $project->enableKey();
            } elseif(!empty($collab)) {
                $delCollab = new Collab();
                $delCollab->set_id($collab[0]['id'])->remove();
            } else {
                echo "Demande non existante";
            }
            header("Location: /setting");
        } else {
            $this->render("default/error");
        }
    }
}