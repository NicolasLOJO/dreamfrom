<?php

class EventController extends Controller {

    public static function getEvent(){
        $allEvent = [];
        $event = new DAOProject();
        $project = $event->customData("SELECT * FROM project WHERE accepted = 1 ORDER BY created_date DESC LIMIT 2");
        $allEvent['project'] = $project;
        $collab = $event->customData("SELECT project.*, user.username FROM project INNER JOIN collab ON collab.id_project = project.id INNER JOIN user ON collab.id_user = user.id WHERE project.accepted = 1 ORDER BY collab.added_date DESC LIMIT 2");
        $allEvent['collab'] = $collab;
        $old = $event->customData("SELECT * FROM project WHERE accepted = 1 ORDER BY created_date ASC LIMIT 2");
        $allEvent['old'] = $old;
        $nouser = $event->customData("SELECT * FROM project WHERE id_user = 0 ORDER BY created_date DESC LIMIT 2");
        $allEvent['nouser'] = $nouser;
        
        return $allEvent;
    }
}