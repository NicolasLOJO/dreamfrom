<?php

class UserController extends Controller {

    public function getLogin(){
        $reg = new DAORegion();
        $reg = $reg->getAll();
        $data = array("region" => $reg);
        $this->render("user/login_home", $data);
    }

    public function getRegister($status = null){
        $region = new DAORegion();
        $departement = new DAODepartement();
        if($status){
            $data = array("departement" => $departement->getAll(), "status" => $status);
        } else {
            $data = array("departement" => $departement->getAll());
        }
        $this->render("user/register_home", $data);
    }

    public function userRegister(){
        $post = $this->inputPost();
        if(is_null($this->userExist())){
            if(!empty($post['username'])){
                if(!empty($post['nom']) && !empty($post['prenom'])){
                    $user = $this->addUser();
                }
            }
        }
    }

    public function userLogin(){
        $post = $this->inputPost();
        $user = $this->userExist();
        if(!empty($post['username']) && !empty($post['userpass'])){
            if(!is_null($user)){
                if(password_verify($post['userpass'], $user['password'])){
                    $_SESSION['user_id'] = $user['id'];
                    $_SESSION['username'] = $user['username'];
                    $_SESSION['status'] = $user['status'];
                    $_SESSION['avatar'] = $user['avatar'];
                    $data = array("status" => "ok", "response" => "200");
                } else {
                    $data = array("status" => "bad", "response" => "204");
                }
            } else {
                    $data = array("status" => "bad", "response" => "204");
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function userLogout(){
        session_destroy();
        header("Location: /");
        $this->render("default/home");
    }

    private function userExist(){
        $post = $this->inputPost();
        $user = new DAOUser();
        $user = $user->getAll();
        foreach($user as $key => $value){
            if(isset($post['username'], $value['username']) && !empty($post['username']) && !empty($value['username'])){
                if(strtolower($post['username']) == strtolower($value['username'])){
                    return $value;
                }
            }
        }
    }

    private function addUser(){
        $post = $this->inputPost();
        $user = new User();
        if($this->verifPass()){
            $hashed = password_hash($post['userpass'], PASSWORD_DEFAULT);
            $user->set_nom($post['nom'])->set_prenom($post['prenom'])->set_username($post['username'])->set_password($hashed)->set_id_departement($post['dep']);
            if(isset($post['private'])){
                $user->set_private($post['private']);
            }
            if(isset($post['contacter'])){
                $user->set_contact($post['contacter']);
            }
            $user->update();
            header("Location: /login");
        } else {
            $status = "Mot de passe ne correspond pas";
            $this->getRegister($status);
        }
    }

    private function verifPass(){
        $post = $this->inputPost();
        if(isset($post['userpass'], $post['passconfirm']) && $post['userpass'] == $post['passconfirm']){
            if(!empty($post['userpass']) && !empty($post['passconfirm'])){
                return true;
            } else {
                $status = "Veuillez renseigner un mot de passe";
                $this->getRegister($status);
            }
        }
    }
}