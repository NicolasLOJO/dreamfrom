<?php

class MemberController extends Controller {

    public function getMember(){
        if(isset($_SESSION['status'])){
            $user = new DAOUser();
            $data = array("user" => $user->getAllBy("private = 0"), 'event' => EventController::getEvent());
            $this->render("member/member_home", $data);
        } else {
            $data = array("status" => "Vous devez être connecté pour voir les membres");
            $this->render("user/login_home", $data);
        }
    }

    public function getProfil($data = null){
        if(isset($_SESSION['status'])){
            $user = new User();
            $user = $user->set_id($_SESSION['user_id'])->load();
            $path = "/asset/picture/avatar/";
            if($data){
                $departement = new DAODepartement();
                $departement = $departement->getAll();
                $region = new DAORegion();
                $region = $region->getAll();
                $data = array("load" => $data, "user" => $user, "path" => $path, "region" => $region, "departement" => $departement, 'event' => EventController::getEvent());
            } else {
                $departement = new Departement();
                $departement = $departement->set_id($user->get_id_departement())->load();
                $region = new Region();
                $region = $region->set_id($departement->get_id_region())->load();
                $data = array("load" => "./views/modules/profil.php", "user" => $user, "path" => $path, "region" => $region, "departement" => $departement, 'event' => EventController::getEvent());
            }
            $this->render("default/home", $data);
        } else {
            $this->render("default/error");
        } 
    }

    public function updatePass(){
        if(isset($_SESSION['status'])){
            $data = array('load' => "./views/modules/update_pass.php");
            $this->render("default/home", $data);
        } else {
            $this->render("default/error");
        }
    }

    public function changePass(){
        if(isset($_SESSION['status'])){
            $post = $this->inputPost();
            $user = new User();
            $user = $user->set_id($_SESSION['user_id'])->load();
            if(password_verify($post['oldpass'], $user->get_password())){
                if($post['newpass'] == $post['confirm_newpass']){
                    if($post['newpass'] != $post['oldpass']){
                        $hash = password_hash($post['newpass'], PASSWORD_DEFAULT);
                        $user = new User();
                        $user->set_id($_SESSION['user_id'])->set_password($hash)->update();
                    }
                }
            }
            header("Location: /setting");
        } else {
            $this->render('default/error');
        }
    }

    public function updateMember(){
        if(isset($_SESSION['status'])){
            $view = "./views/modules/profil_update.php";
            $this->getProfil($view);
        } else {
            $this->render("default/error");
        }
    }

    public function addUpdateMember(){
        if(isset($_SESSION['status'])){
            $post = $this->inputPost();
            $user = new User();
            $user->set_id($_SESSION['user_id']);
            if(!empty($_FILES['avatar']['name'])){
                $avatar = $this->addAvatar();
                $user->set_avatar($avatar);
                $user->update();
            }
            if(isset($post['private'])){
                $user->set_private($post['private']);
            } else {
                $user->set_private("0");
            }
            if(isset($post['contact'])){
                $user->set_contact($post['contact']);
            } else {
                $user->set_contact("0");
            }
            $user->set_id_departement($post['dep'])->update();
            header("Location: /setting");
        } else {
            $this->render('default/error');
        }
    }

    private function addAvatar(){
        if(isset($_SESSION['status'])){
            if($_FILES['avatar']['size'] <= 2097152){
                $filename = strtolower($_FILES['avatar']['name']);
                $extension = preg_split("[/\\.]", $filename);
                $n = count($extension)-1;
                $exts = $extension[$n];
                $name = $_SESSION['user_id'].".".$exts;
                $dir = "./asset/picture/avatar/".$name;
                $dimension = getimagesize($_FILES['avatar']['tmp_name']);
                move_uploaded_file($_FILES['avatar']['tmp_name'], $dir);
                $this->cropAvatar(250, 250, $dir, $dir);
                return $name;
            }
        } else {
            $this->render("default/error");
        }
    }

    private function cropAvatar($max_width, $max_height, $source_file, $dst_dir, $quality = 80){
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];
    
        switch($mime){
            case 'image/gif':
                $image_create = "imagecreatefromgif";
                $image = "imagegif";
                break;
    
            case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";
                $quality = 7;
                break;
    
            case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";
                $quality = 80;
                break;
    
            default:
                return false;
                break;
        }
        
        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);
        
        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if($width_new > $width){
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        }else{
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }
        
        $image($dst_img, $dst_dir, $quality);
    
        if($dst_img)imagedestroy($dst_img);
        if($src_img)imagedestroy($src_img);
    }
}