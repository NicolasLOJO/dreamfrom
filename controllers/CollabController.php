<?php

class CollabController extends Controller {

    public function addCjghjghollab($id){
        if(isset($_SESSION['status'])){
            $project = new Project();
            $project->set_id($id);
            $project = $project->load();
            $user = new User();
            $user->set_id($project->get_id_user());
            $user = $user->load();
            if($user != true){
                $user = "En attente de reprise.";
            }
            $collab = new DAOCollab();
            $join = array("user" => "collab.id_user = user.id");
            $data = array('load' => './views/modules/add_collab.php',"projet" => $project, "user" => $collab->getAllBy("id_project = $id",false,  $join), "boss" => $user, 'event' => EventController::getEvent());
            $this->render("default/home", $data);
        } else {
            $this->render("default/error");
        }
    }

    public function addCollab($id){
        if(isset($_SESSION['status'])){
            $post = $this->inputPost();
            $user = new DAOUser();
            $user = $user->getAllBy("username = ".$post['name']);
            if($this->is_notif($user, $id) == false){
                $notif = new Notif();
                $notif->set_id_user($user[0]['id'])->set_id_project($id)->set_type('collab')->set_expediteur($_SESSION['user_id'])->set_lien('/collab/')->set_status('member')->update();
            } else {
                echo json_encode("deja envoyé");
            }
        } else {
            $this->render("default/error");
        }
    }

    public function acceptCollab($notif){
        if(isset($_SESSION['status'])){
            $validation = $notif;
            $notif = new Notif();
            $notif = $notif->set_id($validation)->load();
            if(!empty($notif) && $notif->get_id_user() == $_SESSION['user_id'] && $notif->get_type() == 'collab' && is_null($notif->get_accepted())){
                $collab = new Collab();
                $collab->set_id_user($_SESSION['user_id'])->set_id_project($notif->get_id_project())->update();
                $update = new Notif();
                $update->set_id($validation)->set_accepted("1")->set_hidden("1")->update();
                header("Location: /");
            } else {
                echo "Demande non existante";
            }
        } else {
            $this->render("default/error");
        }
    }

    public function declineCollab($notif){
        if(isset($_SESSION['status'])){
            $validation = $notif;
            $notif = new Notif();
            $notif = $notif->set_id($validation)->load();
            if(!empty($notif) && $notif->get_id_user() == $_SESSION['user_id'] && $notif->get_type() == 'collab' && is_null($notif->get_accepted())){
                $update = new Notif();
                $update->set_id($validation)->remove();
                header("Location: /");
            } else {
                echo "Demande non existante";
            }
        } else {
            $this->render("default/error");
        }
    }

    private function is_notif($user, $id){
        $verif = new DAONotif();
        foreach($verif->getAll() as $value){
            if($value['expediteur'] == $_SESSION['user_id'] && $value['id_user'] == $user[0]['id'] && $value['id_project'] == $id){
                return true;
            }
        }
        return false;
    }
}