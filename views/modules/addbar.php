<div class="col-9 mt-3">
    <h2>Nouveau Post</h2>
    <form method="POST">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="">Catégorie</span>
            </div>
            <input readonly type="text" class="form-control" value="<?= $cat->get_name() ?>">
            <input type="hidden" name="cat" value="<?= $cat->get_id() ?>">
            <div class="input-group-prepend">
                <span class="input-group-text" id="">Sous-catégorie</span>
            </div>
            <input readonly type="text" class="form-control" value="<?= $sub->get_name() ?>">
            <input type="hidden" name="sub" value="<?= $sub->get_id() ?>">
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="Bar_titre">Titre</span>
            </div>
            <input type="text" name="bar_title" class="form-control" id="Bar_title" aria-describedby="Bar_titre">
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Votre contenu</span>
            </div>
            <textarea class="form-control" name="content" aria-label="With textarea"></textarea>
        </div>
        <?php if(isset($status)){ ?>
        <p><?= $status ?></p>
        <?php } ?>
        <input type="submit" class="btn btn-primary" value="Ajouter">
    </form>
</div>