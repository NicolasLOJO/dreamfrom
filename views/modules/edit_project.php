<p>Edition</p>
<div class="container border rounded">
    <input type="hidden" id="id_project" value="<?= $projet->get_id() ?>">
    <div id="row_edit_project" class="row justify-content-center border-bottom mt-3">
        <div class="input-group justify-content-center input-group-lg ml-3 mr-3 mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm">Titre</span>
            </div>
            <input class="form-control" type="text" name="title" id="title_edit_project" value="<?= $projet->get_titre() ?>">
            <div class="input-group-append">
                <span id="valid_update_project" class="btn btn-outline-secondary">Modifier</span>
            </div>
        </div>
    </div>
    <div class="row mt-3 justify-content-center">
        <h4>Auteur</h4>
    </div>
    <div class="row justify-content-center">
        <img src="/asset/picture/avatar/<?= $boss->get_avatar() ?>" alt="avatar" style="max-width: 12rem; max-height: 12rem;" class="img-thumbnail rounded-circle mt-3">
    </div>
    <div class="row justify-content-center border-bottom">
        <h4><?= $boss->get_username() ?></h4>
    </div>
    <?php if(isset($user)){ ?>
    <div class="row justify-content-center mt-3">
        <h4>Collaboratueur</h4>
    </div>
    <div class="row justify-content-center border-bottom">
        <?php foreach($user as $member){ ?>
            <div class="col-lg-3 col-sm-12 text-center">
                <img src="/asset/picture/avatar/<?= $member['avatar'] ?>" alt="avatar" style="max-width: 8rem;" class="img-thumbnail rounded-circle mt-3">
                <p style="font-weight: bold;"><?= $member['username'] ?></p>
            </div>
        <?php } ?>
        <?php foreach($notif as $pending){ ?>
            <?php if(is_null($pending['accepted'])){ ?>
            <div class="col-lg-3 col-sm-12 text-center">
                <img src="/asset/picture/avatar/<?= $pending['avatar'] ?>" alt="avatar" style="max-width: 8rem;" class="img-thumbnail rounded-circle mt-3">
                <p style="font-weight: bold;"><?= $pending['username'] ?></p>
                <p>En attente</p>
            </div>
        <?php }} ?>
        <div class="col-lg-3 col-sm-12 text-center">
            <img id="edit_addcollab" src="/asset/picture/avatar/add.jpg" alt="avatar" style="max-width: 8rem;" class="img-thumbnail rounded-circle mt-3">
            <p style="font-weight: bold;">Ajouter</p>
            <div id="show-addcollab" style="display: none;" class="mb-3">
                <input type="hidden" id="button_choice" value="collab">
                <input type="text" id="name_addcollab" class="form-control mb-3" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                <span style="display: none;" id="valid_addcollab" class="btn btn-primary">Ajouter</span>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="row justify-content-center mt-3">
        <h4>Description</h4>
    </div>
    <div class="row mt-3 ml-3 mr-3">
        <div class="input-group mb-3">
            <textarea class="form-control" name="desc" id="desc_edit_project"><?= $projet->get_content() ?></textarea>
            <div class="input-group-append">
                <button id="valid_desc_project" class="btn btn-outline-secondary" type="button">Envoyer</button>
            </div>
        </div>
    </div>
    <?php if(isset($task)){ ?>
    <div class="row justify-content-center mt-3 pt-3 border-top">
        <h4>Tâches</h4>
    </div>
    <div class="row mt-3 ml-3 mb-3">
        <?php foreach($task as $tache){ if($projet->get_id_user() == $_SESSION['user_id']){?>
            <div class="form-check">
                <input class="form-check-input" name="task" type="checkbox" id="tache<?= $tache['id'] ?>" value="">
                <label class="form-check-label" for="tache<?= $tache['id'] ?>"><?= $tache['content'] ?></label>
            </div>
        <?php } else { ?>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" disabled>
                <label class="form-check-label"><?= $tache['content'] ?></label>
            </div>
        <?php }} ?>
    </div>
    <?php } ?>
    <div class="row ml-3 mb-3">
        <input type="text" id="addTask" name="addtask" placeholder="Ajouter taches">
    </div>


<div class="modal fade" id="success_valid_project" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div id="color_modal_project" class="modal-content text-white">
            <div id="project_modal_content" class="modal-body ml-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                
            </div>
        </div>
    </div>
</div>

</div>