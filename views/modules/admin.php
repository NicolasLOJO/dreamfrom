<div class="col-lg-9 col-sm-12">
    <div class="card shadow">
        <h5 class="card-header bg-dark text-white">Administration</h5>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6 col-sm-12 mb-3">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="card-title">Membres</h5>
                            <p class="card-text">Il y a <?= $user[0]['count'] ?> personnes d'inscrite sur le site.</p>
                            <a href="/admin/membres" class="btn btn-primary">Voir les membres</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="card-title">Projets</h5>
                            <p class="card-text">Il y a <?= $project[0]['count'] ?> projets d'inscrit.</p>
                            <a href="/admin/projects" class="btn btn-primary">Voir les projets</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>