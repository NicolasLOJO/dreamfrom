<div class="col-12">
    <a class="btn shadow-sm btn-primary mb-3" href="/Projects/add">Ajouter</a>
</div>
<?php foreach($projet as $value){ ?>
    <div class="card shadow text-center mb-3">
        <div class="card-body">
            <h5 class="card-title"><?= $value['titre'] ?></h5>
            <p class="card-text"><?= substr($value['content'], 0, 40) ?>...</p>
            <a href="/Projects/<?= $value['id'] ?>" class="btn btn-primary">Voir le projet</a>
            <a href="/setting/edit/<?= $value['id'] ?>" class="btn btn-primary">Editer</a>
            <a href="/setting/quit/<?= $value['id'] ?>" class="btn btn-primary">Quitter</a>
        </div>
        <div class="card-footer text-muted">
            Boss
        </div>
    </div>
<?php } ?>
<?php foreach($collab as $value){ ?>
    <div class="card shadow text-center mb-3">
        <div class="card-body">
            <h5 class="card-title"><?= $value['titre'] ?></h5>
            <p class="card-text"><?= substr($value['content'], 0, 40) ?>...</p>
            <a href="/Projects/<?= $value['id'] ?>" class="btn btn-primary">Voir le projet</a>
            <a href="/setting/quit/<?= $value['id'] ?>" class="btn btn-primary">Quitter</a>
        </div>
        <div class="card-footer text-muted">
            Collaboration
        </div>
    </div>
<?php } ?>