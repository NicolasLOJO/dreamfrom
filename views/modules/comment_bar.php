<div class="col-lg-9 col-sm-12 mt-3">
    <div class="container">
        <div class="row border rounded align-items-center mb-3">
            <div class="col-lg-3 col-sm-12 border-right">
                <img src="/asset/picture/avatar/<?= $bar['avatar'] ?>" alt="..." class="img-thumbnail rounded-circle mt-3">
                <ul class="list-group list-group-flush mt-3">
                    <li class="list-group-item">Crée par: <?= $bar['username'] ?></li>
                    <li class="list-group-item">Le: <?= date("d-m-Y", strtotime($bar['created_date'])) ?></li>
                    <li class="list-group-item"><?php if($bar['contact'] == 0){?><a href="/message/<?= $bar['id_author'] ?>">Contacter</a><?php } ?></li>
                </ul>
            </div>
            <div class="col-lg-9 col-sm-12">
                <h2 class="mt-3 border-bottom"><?= $bar['titre'] ?></h2>
                <p class="text-center"><?= $bar['content'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form method="POST">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Message</span>
                        </div>
                        <textarea class="form-control" name="answer" aria-label="With textarea"></textarea>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit">Envoyer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>