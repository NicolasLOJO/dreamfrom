<div class="card shadow">
  <h5 class="card-header bg-dark text-white">Mon profil</h5>
  <div class="card-body">
    <img style="width: 18rem;" class="card-img-top mb-3 rounded-circle" src="<?= $path.$user->get_avatar() ?>" alt="Card image cap">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Nom : <?= $user->get_nom() ?></li>
        <li class="list-group-item">Prénom : <?= $user->get_prenom() ?></li>
        <li class="list-group-item">Pseudo : <?= $user->get_username() ?></li>
        <li class="list-group-item">Région : <?= $region->get_nom() ?></li>
        <li class="list-group-item mb-3">Département : <?= $departement->get_nom() ?></li>
    </ul>
    <a href="/setting/pass/edit" class="btn btn-primary">Changer le mot de passe</a>
    <a href="/setting/update" class="btn btn-primary">Changer les informations</a>
  </div>
</div>