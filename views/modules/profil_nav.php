<div div="nav-profil" class="col-lg-2 col-sm-12 col-md-12">
<ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link" href="/"><i class="material-icons text-secondary align-middle mr-3">folder</i>Mes projets</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/messages"><i class="material-icons text-secondary align-middle mr-3">mail</i>Mes messages</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/setting"><i class="material-icons text-secondary align-middle mr-3">settings</i>Réglages</a>
  </li>
  <?php if($_SESSION['status'] == 'admin'){ ?> 
    <li class="nav-item">
        <a class="nav-link" href="/admin"><i class="material-icons text-secondary align-middle mr-3">supervised_user_circle</i>Administration</a>
    </li>
  <?php } ?>
</ul>
</div>