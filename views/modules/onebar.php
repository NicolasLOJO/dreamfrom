<div class="col-lg-9 col-sm-12 col-md-8 mt-3">
    <div class="container">
        <div class="row border rounded align-items-center">
            <div class="col-lg-3 col-sm-4 border-right">
                <img src="/asset/picture/avatar/<?= $bar['avatar'] ?>" alt="..." class="img-thumbnail rounded-circle mt-3">
                <ul class="list-group list-group-flush mt-3">
                    <li class="list-group-item">Crée par: <?= $bar['username'] ?></li>
                    <li class="list-group-item">Le: <?= date("d-m-Y", strtotime($bar['created_date'])) ?></li>
                    <li class="list-group-item"><?php if($bar['contact'] == 0){?><a href="/message/<?= $bar['id_author'] ?>">Contacter</a><?php } ?></li>
                </ul>
            </div>
            <div class="col-lg-9 col-sm-8">
                <h2 class="mt-3 border-bottom"><?= $bar['titre'] ?></h2>
                <p class="text-center"><?= $bar['content'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
            <?php if(isset($_SESSION['user_id'])){ ?>
                <a class="btn btn-primary mb-3 mt-3 float-right" href="/Bar/<?= $bar['id'] ?>/comment">Répondre</a>
            <?php } ?>
            </div>
        </div>
    </div>
    <?php if(!empty($com)){ ?>
        <div class="container border rounded">
            <?php foreach($com as $value){ ?>
                <div class="row align-items-center border-bottom">
                    <div class="col-lg-3 col-sm-4 border-right">
                        <img src="/asset/picture/avatar/<?= $value['avatar'] ?>" alt="..." class="img-thumbnail rounded-circle mt-3 w-50 h-50">
                        <ul class="list-group list-group-flush mt-3">
                            <li class="list-group-item">Crée par: <?= $value['username'] ?></li>
                            <li class="list-group-item">Le: <?= date("d-m-Y", strtotime($bar['created_date'])) ?></li>
                            <li class="list-group-item"><?php if($value['contact'] == 0){?><a href="/message/<?= $value['id_author'] ?>">Contacter</a><?php } ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-9 col-sm-8">
                        <p class="text-center"><?= $value['content'] ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>