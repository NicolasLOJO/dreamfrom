<div class="col-lg-9 col-sm-12">
    <div class="card shadow">
        <h5 class="card-header bg-dark text-white">Liste des membres</h5>
        <div class="card-body">
            <div class="card-deck">
                <?php foreach($user as $member){ ?>
                    <div class="col-lg-3 col-sm-6 mb-3">
                        <div class="card">
                            <img class="img-thumbnail rounded-circle card-img-top" src="/asset/picture/avatar/<?= $member['avatar'] ?>" alt="Card image cap">
                            <h5 class="text-center mt-3"><?= $member['username'] ?></h5>
                            <div class="text-center btn-maj-member">
                                <form method="POST" class="text-start form-member" style="display: none;">
                                    <div class="card-body">
                                        <h5 class="card-title">Pseudo: </h5>
                                        <input class="form-control" type="text" name="pseudo" id="pseudo" value="<?= $member['username'] ?>">
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Nom:
                                            <input class="form-control" type="text" name="nom" id="nom" value="<?= $member['nom'] ?>">
                                        </li>
                                        <li class="list-group-item">Prénom:
                                            <input class="form-control" type="text" name="prenom" id="prenom" value="<?= $member['prenom'] ?>">
                                        </li>
                                        <li class="list-group-item">Status de base: <?= $member['status'] ?></li>
                                        <li class="list-group-item">Status:
                                            <select name="status" id="status">
                                                <?php if($_SESSION['user_id'] == $member['id'] && $member['status'] == 'admin'){ ?>
                                                    <option value="admin">Admin</option>
                                                <?php }else{ ?>
                                                    <option value="admin">Admin</option>
                                                    <option selected value="member">Membre</option>
                                                <?php } ?>
                                            </select>
                                        </li>
                                    </ul>
                                    <input type="hidden" name="id" value="<?= $member['id'] ?>">
                                    <div class="card-body">
                                        <input type="submit" class="btn btn-primary mb-3" value="Update">
                                        <a href="/admin/membres/<?= $member['id'] ?>/delete" class="btn btn-primary">Supprimer</a>
                                    </div>
                                </form>
                                <div class="maj-member-icon-div bg-primary text-center text-white rounded-bottom"><i class="maj-member-icon material-icons">expand_more</i></div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>