<div class="col-9">
<h2>Resultat de la recherche</h2>
    <form method="POST">
        <input type="text" minlength="3" name="search" placeholder="name, project, etc...">
        <input type="submit" value="Chercher">
    </form>
    <div class="card-columns">
    <?php if(!empty($result)){ ?>
        <?php foreach($result as $value){ ?>
            <?php if(is_null($value['username'])){ ?>
                <div class="card mb-3" style="min-width: 14rem; max-width: 18rem;">
                    <div class="card-header">Projet</div>
                    <div class="card-body">
                        <h5 class="card-title"><?= $value['titre'] ?></h5>
                        <p class="card-text"><?= substr($value['content'], 0, 20) ?>...</p>
                        <a href="/Projects/<?= $value['id_project'] ?>" class="btn btn-primary">Voir le projet</a>
                    </div>
                </div>
                <?php } elseif(is_null($value['id_project'])){ ?>
                <div class="card mb-3" style="min-width: 14rem; max-width: 18rem;">
                    <div class="card-header"><?= $value['username'] ?></div>
                    <img class="mx-auto d-block card-img-top w-50 rounded-circle img-thumbnail" style="max-width: 18rem;" src="/asset/picture/avatar/<?= $value['avatar'] ?>" alt="Card image cap">
                    
                </div>
                <?php } ?>
        <?php }} else { ?>
            <div class="card mb-3">
            <div class="card-header">Resultat</div>
                <div class="card-body">
                    <h5 class="card-title">Pas de résultat</h5>
                    <p class="card-text">Essayé une autre recherche</p>
                </div>
            </div>
        <?php } ?>
    </div>
</div>