<div class="col-lg-9 col-sm-12">
    <div class="card shadow">
        <h5 class="card-header bg-dark text-white">Liste des projets</h5>
        <div class="card-body">
            <div class="card-deck">
                <?php foreach($project as $value){ ?>
                    <div class="col-lg-6 col-sm-12">
                        <div class="card mb-3">
                            <h5 class="card-header bg-primary text-white">Crée par : <?= $value['username'] ?></h5>
                            <div class="card-body">
                                <h5 class="card-title"><?= $value['titre'] ?></h5>
                                <p class="card-text"><?= substr($value['content'], 0, 20) ?>...</p>
                                <a href="/Projects/<?= $value['id'] ?>" class="btn btn-primary">Voir le projet</a>
                                <a href="/admin/projects/<?= $value['id'] ?>/delete" class="btn btn-primary">Supprimer</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>