<div class="card-deck">
<?php if(isset($message)){ foreach($message as $pm) {
    if($pm['readed_message'] == false) { 
        if($pm['deleted_destinataire'] == false) { ?>
            <div class="card shadow col-lg-6 col-sm-12">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">Expediteur</h5>
                    <h6 class="card-subtitle mb-2 text-muted font-weight-bold"><?= htmlspecialchars($pm['username']) ?></h6>
                    <p class="card-text font-weight-bold"><?= htmlspecialchars($pm['content']) ?>.</p>
                    <button type="button" class="btn btn-primary modal-test">Répondre</button>
                    <a href="/messages/<?= $pm['id'] ?>/delete" class="card-link">Supprimer</a>
                    <input type="hidden" name="dest" class="dest" value="<?= $pm['id_user'] ?>">
                    <input type="hidden" name="object" class="object" value="<?= $pm['object'] ?>">
                </div>
            </div>
    <?php }
    } else { 
        if($pm['deleted_destinataire'] == false) {?>
            <div class="card shadow col-lg-6 col-sm-12">
                <div class="card-body">
                    <h5 class="card-title">Expediteur</h5>
                    <h6 class="card-subtitle mb-2 text-muted"><?= htmlspecialchars($pm['username']) ?></h6>
                    <p class="card-text"><?= htmlspecialchars($pm['content']) ?>.</p>
                    <button type="button" class="btn btn-primary modal-test">Répondre</button>
                    <a href="/messages/<?= $pm['id'] ?>/delete" class="card-link">Supprimer</a>
                    <input type="hidden" name="dest" class="dest" value="<?= $pm['id_user'] ?>">
                    <input type="hidden" name="object" class="object" value="<?= $pm['object'] ?>">
                </div>
            </div>
    <?php }} ?>
<?php }} ?>
</div>

<div class="modal fade" id="MessageModal" tabindex="-1" role="dialog" aria-labelledby="MessageModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-message" method="POST">
            <input readonly type="hidden" class="form-control mb-2" name="dest" id="destinataire" value="">
            <input readonly type="text" class="form-control mb-2" name="username" id="username" value="">
            <input type="hidden" name="object" id="object" value="">
            <p id="content-message"></p>
            <textarea class="form-control" name="answer" id="answer" placeholder="Réponse"></textarea>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
        <button id="form-submit" type="button" class="btn btn-primary">Envoyer<i class="ml-3 material-icons text-white align-middle">send</i></button>
      </div>
    </div>
  </div>
</div>