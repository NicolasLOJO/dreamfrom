<div class="col-lg-9 col-sm-12">
    <div class="card shadow">
        <h5 class="card-header bg-dark text-white">Configuration</h5>
        <div class="card-body">
            <form method="POST">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-primary text-white" id="">Catégorie</span>
                    </div>
                    <input type="text" class="form-control" placeholder="Nouvelle catégorie" name="cat" id="cat">
                    <div class="input-group-append">
                        <input class="btn btn-outline-secondary" type="submit" value="Ajouter">
                    </div>
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-primary text-white" id="">Sous-catégorie</span>
                    </div>
                    <input type="text" name="subcat" class="form-control" placeholder="Nouvelle sous-catégorie">
                    <select name="id_cat" class="custom-select" id="inputGroupSelect01">
                        <option selected value="0">Choisir la catégorie</option>
                        <?php foreach($subcat as $value){ ?>
                            <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                        <?php } ?>
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="Submit">Ajouter</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>