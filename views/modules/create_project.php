<p>Ajouter un projet</p>
<form action="/Projects" method="POST">
<div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="bg-primary text-white input-group-text">Titre</span>
        </div>
        <input name="ptitle" class="form-control" aria-label="titre">
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="bg-primary text-white input-group-text">Catégorie</span>
        </div>
        <select name="pcat" id="custom-cat" class="custom-select">
            <option value="0">Votre choix...</option>
            <?php foreach($cat as $value){ ?> 
                <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
            <?php } ?>
        </select>
        <select style="display: none;" name="psubcat" id="custom-sub" class="custom-select"></select>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="bg-primary text-white input-group-text">Description</span>
        </div>
        <textarea name="pdesc" class="form-control" aria-label="Description"></textarea>
    </div>
    <input type="submit" class="btn btn-primary" value="Envoyer">
</form>