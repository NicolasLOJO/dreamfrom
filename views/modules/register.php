<div>
    <form action="/register" method="POST">
        <label for="region">Region: </label>
        <label for="region">Departement: </label>
        <select name="dep" id="dep">
        <?php foreach($departement as $value){ ?>
            <option value="<?= $value['id'] ?>"><?= $value['nom'] ?></option>
        <?php } ?>
        </select>
        <label for="nom">Nom:</label>
        <input type="text" name="nom" id="nom" placeholder="Nom">
        <label for="prenom">Prénom:</label>
        <input type="text" name="prenom" id="prenom" placeholder="Prénom">
        <label for="username">Pseudo:</label>
        <input type="text" name="username" id="username" placeholder="Pseudonyme">
        <label for="userpass">Mot de passe:</label>
        <input type="password" name="userpass" id="userpass" placeholder="Mot de passe">
        <label for="passconfirm">Confirmation:</label>
        <input type="password" name="passconfirm" id="passconfirm" placeholder="Confirmation de mot de passe">
        <input type="checkbox" name="private" value="1">Rendre le profil privé
        <input type="checkbox" name="contacter" value="1">Je ne souhaite pas pouvoir être contacté par les membres
        <input type="submit" value="S'enregister">
    </form>
    <a href="/">Retour à l'acceuil</a>
    <?php if(isset($status)){
        echo $status;
    } ?>
</div>