<div class="col-lg-9 col-sm-12">
<h2>Membres</h2>
<div class="card-deck">
<?php foreach($user as $member) { ?>
<div class="col-lg-4 col-sm-6 mb-3">
    <div class="card" >
        <div class="card-header">
            <?= $member['username'] ?>
        </div>
    <img class="card-img-top rounded-circle img-thumbnail" src="/asset/picture/avatar/<?= $member['avatar'] ?>" alt="Card image cap">
        <div class="card-footer">
            <?php if($member['contact'] == 0 && $_SESSION['user_id'] != $member['id']){ ?>
                <a href="/message/<?= $member['id'] ?>">Contacter</a>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>
</div>
</div>

<!--style="min-width: 12rem; max-width: 16rem;"-->