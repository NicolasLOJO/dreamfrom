<nav class="navbar shadow mb-3 sticky-top navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="/">
        <img src="/asset/picture/logo/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
        DreamFrom
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <?php if($_SERVER['REQUEST_URI'] == "/"){ ?>
                <li class="nav-item active"><a class="nav-link" href="/">Home</a></li>
            <?php } else { ?>
                <li class="nav-item"><a class="nav-link" href="/">Home</a></li>
            <?php } if($_SERVER['REQUEST_URI'] == "/Projects"){ ?>
                <li class="nav-item active"><a class="nav-link" href="/Projects">Projets</a></li>
            <?php } else { ?>
                <li class="nav-item"><a class="nav-link" href="/Projects">Projets</a></li>
            <?php } if($_SERVER['REQUEST_URI'] == "/member"){ ?>
                <?php if(isset($_SESSION['status'])){ ?>
                    <li class="nav-item active"><a class="nav-link" href="/member">Membres</a></li>
                <?php } ?>
            <?php }else{ ?>
                <?php if(isset($_SESSION['status'])){ ?>
                    <li class="nav-item"><a class="nav-link" href="/member">Membres</a></li>
                <?php } ?>
            <?php } ?>
            <?php if($_SERVER['REQUEST_URI'] == "/Bar"){ ?>
                <li class="nav-item active"><a class="nav-link" href="/Bar">Bar</a></li>
            <?php } else { ?>
                <li class="nav-item"><a class="nav-link" href="/Bar">Bar</a></li>
            <?php } ?>
        </ul>
        <?php if(isset($_SESSION['username'])){ ?>
        <a class="navbar-brand ml-auto" href="/setting">
            <?= $_SESSION['username']; ?>
            <img src="/asset/picture/avatar/<?= $_SESSION['avatar'] ?>" width="30" height="30" class="d-inline-block rounded-circle align-top" alt="">
        </a>
        <?php } ?>
        <ul class="navbar-nav">
            <?php if(isset($_SESSION['user_id'])){ ?>
                <li id="home-notif" class="nav-item"><i id="icon-notif" class="nav-link material-icons text-white align-middle ml-3">notifications_none</i>
                </li>
                <div style="display: none;" id="notif-space" class="pb-3 col-3 notif bg-light border rounded position-absolute">
                </div>
                <li class="nav-item"><a class="nav-link" href="/logout">Logout<i class="material-icons text-white align-middle ml-3">exit_to_app</i></a></li>
            <?php }else{ ?>
                <li class="nav-item"><a class="nav-link" href="/login">Login<i class="material-icons text-white align-middle ml-3">account_box</i></a></li>
            <?php } ?>
        </ul>
    </div>
</nav>
<div class="container-fluid">
<div class="row">