<div class="col-lg-9 col-sm-12 mt-3">
    <?php if(isset($_SESSION['user_id']) && $projet->get_id_user() == $_SESSION['user_id'] || $_SESSION['status'] == 'admin'){ ?>
        <a class="btn btn-primary mb-3" href="/Projects/<?= $projet->get_id() ?>/edit">Editer</a>
    <?php } ?>
    <div class="container border rounded">
        <div class="row justify-content-center border-bottom mt-3">
            <h2><?= $projet->get_titre() ?></h2>
        </div>
        <div class="row mt-3 justify-content-center">
            <h4>Auteur</h4>
        </div>
        <div class="row justify-content-center">
            <img src="/asset/picture/avatar/<?= $boss->get_avatar() ?>" alt="avatar" style="max-width: 12rem; max-height: 12rem;" class="img-thumbnail rounded-circle mt-3">
        </div>
        <div class="row justify-content-center border-bottom">
            <h4><?= $boss->get_username() ?></h4>
        </div>
        <?php if(isset($user)){ ?>
        <div class="row justify-content-center mt-3">
            <h4>Collaboratueur</h4>
        </div>
        <div class="row justify-content-center border-bottom">
            <?php foreach($user as $member){ ?>
                <div class="col-lg-3 col-sm-12 text-center">
                    <img src="/asset/picture/avatar/<?= $member['avatar'] ?>" alt="avatar" style="max-width: 8rem;" class="img-thumbnail rounded-circle mt-3">
                    <p style="font-weight: bold;"><?= $member['username'] ?></p>
                </div>
            <?php } ?>
            <?php foreach($notif as $pending){ ?>
            <?php if(is_null($pending['accepted'])){ ?>
            <div class="col-lg-3 col-sm-12 text-center">
                <img src="/asset/picture/avatar/<?= $pending['avatar'] ?>" alt="avatar" style="max-width: 8rem;" class="img-thumbnail rounded-circle mt-3">
                <p style="font-weight: bold;"><?= $pending['username'] ?></p>
                <p>En attente</p>
            </div>
            <?php }} ?>
        </div>
        <?php } ?>
        <div class="row justify-content-center mt-3">
            <h4>Description</h4>
        </div>
        <div class="row mt-3 ml-3">
            <p><?= $projet->get_content() ?></p>
        </div>
        <?php if(!empty($task)){ ?>
        <div class="row justify-content-center mt-3 pt-3 border-top">
            <h4>Tâches</h4>
        </div>
        <div class="row mt-3 ml-3">
            <?php foreach($task as $tache){ if($projet->get_id_user() == $_SESSION['user_id']){?>
                <p><?= $tache['content'] ?></p>
            <?php } else { ?>
                <p><?= $tache['content'] ?></p>
            <?php }} ?>
        </div>
        <?php } ?>
    </div>
</div>