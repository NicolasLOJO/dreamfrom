<div class="col-lg-3 col-sm-12 col-md-4">
    <div id="event" class="event">
        <?php foreach($event as $key => $value){
            if($key == 'project'){ ?>
            <div class="shadow card mt-3">
                <div class="card-header text-white bg-dark">Nouveau projets</div>
            <?php foreach($value as $news){?>
                <div class="card-body">
                    <h5 class="card-title"><?= $news['titre'] ?></h5>
                    <p class="card-text"><?= substr($news['content'], 0, 30) ?>...</p>
                    <a href="/Projects/<?= $news['id'] ?>">Voir le projets</a>
                </div>
            <?php } ?>
            </div>
            <?php }elseif($key == 'collab'){ ?>
            <div style="display: none;" class="shadow expand-card card mt-3">
                <div class="card-header text-white bg-dark">Nouvelle collaboration</div>
            <?php foreach($value as $news){?>
                <div class="card-body">
                    <h5 class="card-title"><?= $news['username'] ?></h5>
                    <p class="card-text"><?= "dans le projet ".$news['titre'] ?></p>
                    <a href="/Projects/<?= $news['id'] ?>">Voir le projets</a>
                </div>
            <?php } ?>
            </div>
            <?php }elseif($key == 'old'){ ?>
            <div style="display: none;" class="shadow expand-card card mt-3">
                <div class="card-header text-white bg-dark">Vieux projet</div>
            <?php foreach($value as $news){?>
                <div class="card-body">
                    <h5 class="card-title"><?= $news['titre'] ?></h5>
                    <p class="card-text"><?= substr($news['content'], 0, 30) ?>...</p>
                    <a href="/Projects/<?= $news['id'] ?>">Voir le projets</a>
                </div>
            <?php } ?>
            </div>
            <?php }elseif($key == 'nouser'){ ?>
            <div style="display: none;" class="shadow expand-card card mt-3">
                <div class="card-header text-white bg-dark">En attente de reprise</div>
            <?php foreach($value as $news){ ?>
                <div class="card-body">
                    <h5 class="card-title"><?= $news['titre'] ?></h5>
                    <p class="card-text"><?= substr($news['content'], 0, 30) ?>...</p>
                    <a href="/Projects/<?= $news['id'] ?>">Voir le projets</a>
                </div>
            <?php } ?>
            </div>
            <?php } ?>
        <?php } ?>
    </div>
    <div id="expand-event" class="shadow bg-dark text-center text-white rounded-bottom">
        <i id="event-icon" class="material-icons">expand_more</i>
    </div>
</div>
