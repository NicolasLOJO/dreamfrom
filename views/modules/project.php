<div class="col-lg-9 col-sm-12">
    <h2>Projets</h2>
    <form action="/search" method="POST">
        <input type="text" minlength="3" name="search" placeholder="name, project, etc...">
        <input type="submit" value="Chercher">
    </form>
    <div class="accordion" id="list_project">
    <?php foreach($cat as $value){ ?>
        <div class="card">
            <div class="card-header" id="headingOne">
               <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#cat<?= $value['id'] ?>" aria-expanded="true" aria-controls="collapseOne">
                        <?= $value['name'] ?>
                    </button>
                </h5>
            </div>

            <div id="cat<?= $value['id'] ?>" class="collapse" aria-labelledby="headingOne" data-parent="#list_project">
                <div class="card-body">

                <div class="accordion" id="subcat">
                    <?php foreach($sub as $subcat){ if($subcat['id_categories'] == $value['id']) { ?>
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#sub<?= $subcat['id'] ?>" aria-expanded="true" aria-controls="collapseOne">
                                    <?= $subcat['name'] ?>
                                </button>
                            </h5>
                        </div>

                        <div id="sub<?= $subcat['id'] ?>" class="collapse" aria-labelledby="headingOne" data-parent="#subcat">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                <?php foreach($projet as $pro){ if($pro['id_souscategorie'] == $subcat['id'] && $pro['id_categorie'] == $value['id'] && $pro['accepted'] == true){ ?>
                                    <li class="list-group-item"><a href="/Projects/<?= $pro['id'] ?>"><?= $pro['titre'] ?></a></li>
                                <?php }} ?>   
                                </ul> 
                            </div>
                        </div>
                    </div>
                    <?php }} ?>
                </div>

                </div>
            </div>
        </div>
    <?php } ?>
    </div>


</div>

