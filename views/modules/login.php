<div class="container h-75">
  <div class="row h-75 align-items-center justify-content-center">
    <div class="col-lg-6 col-sm-12 ml-3 mr-3 border border-black shadow-lg rounded">
      <div class="row text-center border-bottom rounded justify-content-center">
        <div id="login-div" class="col-6 pb-3 bg-primary pt-3 rounded-left border-right">
          <p class="font-weight-bold h5"><a class="text-white-50" id="login-form-tab" href="#">Connexion</a></p>
        </div>
        <div id="register-div" class="col-6 pb-3 pt-3">
        <p class="font-weight-bold h5"><a id="register-form-tab" href="#">S'inscrire</a></p>
        </div>
      </div>
      <div style="display: none;" id="alert-login" class="alert alert-danger m-3" role="alert"></div>
      <form id="login-form" method="POST">
        <div class="input-group mb-3 mt-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="material-icons">account_circle</i></span>
          </div>
          <input type="text" class="form-control" id="login_username" name="username" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="material-icons">lock</i></span>
          </div>
          <input type="password" class="form-control" placeholder="Password" name="userpass" id="login_password" aria-label="Username" aria-describedby="basic-addon1">
        </div>
        <input type="submit" id="login-submit" class="btn btn-primary float-right mb-3" value="Connexion">
      </form>
      <form action="/register" id="register-form" style="display: none;" method="POST">
        <div class="input-group mb-3 mt-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="material-icons">account_circle</i></span>
          </div>
          <input type="text" class="form-control" id="register_nom" name="nom" placeholder="Nom" aria-label="Username" aria-describedby="basic-addon1">
          <input type="text" class="form-control" id="register_prenom" name="prenom" placeholder="Prénom" aria-label="Username" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="material-icons">account_circle</i></span>
          </div>
          <input type="text" class="form-control" placeholder="Pseudonyme" name="username" id="register-username" aria-label="Username" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="material-icons">lock</i></span>
          </div>
          <input type="password" class="form-control" placeholder="Mot de passe" name="userpass" id="register_password" aria-label="Username" aria-describedby="basic-addon1">
          <input type="password" class="form-control" placeholder="Confirmation de mot de passe" name="passconfirm" id="register_password_confirm" aria-label="Username" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="material-icons">my_location</i></span>
          </div>
          <select class="custom-select" id="register-region">
              <option value="0">Votre choix...</option>
            <?php foreach($region as $option){ ?>
              <option value="<?= $option['id'] ?>"><?= $option['nom'] ?></option>
            <?php } ?>
          </select>
          <select name="dep" style="display: none;" class="custom-select" id="register-dep">
            
          </select>
        </div>
        <input type="submit" id="register-submit" class="btn btn-primary float-right mb-3" value="S'inscrire">
      </form>
    </div>
  </div>
</div>