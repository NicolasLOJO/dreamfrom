<div class="card">
  <h5 class="card-header">Mon profil</h5>
  <div class="card-body">
    <form method="POST" enctype="multipart/form-data">
    <img style="width: 18rem;" class="card-img-top mb-3 rounded-circle" src="<?= $path.$user->get_avatar() ?>" alt="Card image cap">
    <input type="file" name="avatar" id="avatar" accept=".png, .jpeg, .jpg, .gif">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Nom : <?= $user->get_nom() ?></li>
        <li class="list-group-item">Prénom : <?= $user->get_prenom() ?></li>
        <li class="list-group-item">Pseudo : <?= $user->get_username() ?></li>
        <li class="list-group-item mb-3">Département : 
        <select name="dep" id="dep">
        <?php foreach($departement as $value){ if($user->get_id_departement() == $value['id']){ $id = $value['id'] ?>
            <option value="<?= $value['id'] ?>" selected><?= $value['nom'] ?></option>
        <?php } else { ?>
            <option value="<?= $value['id'] ?>"><?= $value['nom'] ?></option>
        <?php }} ?>
        </select>
        </li>
        <div class="form-check">
            <?php if($user->get_private() == 1) {?>
            <input class="form-check-input" name="private" type="checkbox" value="1" id="defaultCheck1" checked>
            <?php } else { ?>
                <input class="form-check-input" name="private" type="checkbox" value="1" id="defaultCheck1">
            <?php } ?>
            <label class="form-check-label" for="defaultCheck1">
                Profil privé
            </label>
        </div>
        <div class="form-check mb-3">
            <?php if($user->get_contact() == 1) {?>
                <input class="form-check-input" name="contact" type="checkbox" value="1" id="defaultCheck2" checked>
            <?php } else { ?>
                <input class="form-check-input" name="contact" type="checkbox" value="1" id="defaultCheck2">
            <?php } ?>
            <label class="form-check-label" for="defaultCheck2">
                Ne pas être contacter
            </label>
        </div>
    </ul>
    <input type="submit" class="btn btn-primary" value="Mettre à jour">
    </form>
  </div>
</div>