<?php foreach($thread as $room){ ?>
    <div class="shadow card mb-3">
        <div class="card-body">
            <h5 class="card-title">Message du Projet "<?= $room['project'] ?>"</h5>
            <?php if($room['newmsg'] != 0) { ?>
                <p>Il y a <?= $room['newmsg'] ?> nouveau message</p>
            <?php }?>
            <a href="/messages/<?= $room['id_project'] ?>" class="card-link">Voir les messages</a>
        </div>
    </div>
<?php } ?>