    </div>
    </div>
    <footer class="bg-dark text-white p-5 mt-5 footer align-bottom">
        <p><a href="/contact">Contactez nous</a></p>
        <div id="foot" class="col-12 text-center">
            <p>Dreamfrom 2018-2018. All Rigth Reserved.</p>
        </div>

        <div class="modal fade" id="test" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="toc-1" class="row text-center">
                        <div id="tic-1" class="align-middle col-4 border tic" style="width: 8rem; height: 8rem;">

                        </div>
                        <div id="tic-2" class="col-4 border tic" style="width: 8rem; height: 8rem;">

                        </div>
                        <div id="tic-3" class="col-4 border tic" style="width: 8rem; height: 8rem;">

                        </div>
                    </div>
                    <div id="toc-2" class="row text-center">
                        <div id="tic-4" class="col-4 border tic" style="width: 8rem; height: 8rem;">

                        </div>
                        <div id="tic-5" class="col-4 border tic" style="width: 8rem; height: 8rem;">

                        </div>
                        <div id="tic-6" class="col-4 border tic" style="width: 8rem; height: 8rem;">

                        </div>
                    </div>
                    <div id="toc-3" class="row text-center">
                        <div id="tic-7" class="col-4 border tic" style="width: 8rem; height: 8rem;">

                        </div>
                        <div id="tic-8" class="col-4 border tic" style="width: 8rem; height: 8rem;">

                        </div>
                        <div id="tic-9" class="col-4 border tic" style="width: 8rem; height: 8rem;">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
                </div>
            </div>
        </div>

    </footer>
</body>
</html>