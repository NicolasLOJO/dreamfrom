<div div="nav-profil" class="col-lg-3 col-sm-12 col-md-12">
<ul class="nav flex-column">
    <li class="nav-item">
    <a class="nav-link" href="/admin"><i class="material-icons text-secondary align-middle mr-3">public</i>Dashboard</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="/admin/membres"><i class="material-icons text-secondary align-middle mr-3">supervised_user_circle</i>Liste des membres</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="/admin/projects"><i class="material-icons text-secondary align-middle mr-3">folder</i>Liste des projets</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/admin/setting"><i class="material-icons text-secondary align-middle mr-3">settings</i>Réglages</a>
    </li>
</ul>
</div>