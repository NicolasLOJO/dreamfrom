<div class="col-9">
<h2>Nouveau message</h2>
<form method="POST">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="">Destinataire & projets</span>
        </div>
        <input readonly name="dest" type="text" class="form-control" value="<?= $user->get_username() ?>">
        <select name="object" class="custom-select" id="inputGroupSelect01">
            <option selected value ="0">Choix du projet</option>
            <?php foreach($pro as $value){ ?>
                <option value="<?= $value['id'] ?>"><?= $value['titre'] ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Votre message</span>
        </div>
        <textarea name="msg" class="form-control" aria-label="Votre message"></textarea>
    </div>
    <input type="submit" class="btn btn-primary" value="Envoyer">
</form>
</div>
