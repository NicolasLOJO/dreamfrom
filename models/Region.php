<?php
class Region extends EntityModel{

    const table = "region";
    
    protected $id;
    protected $nom;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'nom' => 'nom'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_nom($nom){
        $this->nom = $nom;
        return $this;
    }

    public function get_nom(){
        return $this->nom;
    }


}