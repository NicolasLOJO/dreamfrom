<?php
class Task extends EntityModel{

    const table = "task";
    
    protected $id;
    protected $id_user;
    protected $id_project;
    protected $content;
    protected $checked;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'id_user' => 'id_user',
           'id_project' => 'id_project',
           'content' => 'content',
           'checked' => 'checked'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_id_user($id_user){
        $this->id_user = $id_user;
        return $this;
    }

    public function get_id_user(){
        return $this->id_user;
    }

    public function set_id_project($id_project){
        $this->id_project = $id_project;
        return $this;
    }

    public function get_id_project(){
        return $this->id_project;
    }

    public function set_content($content){
        $this->content = $content;
        return $this;
    }

    public function get_content(){
        return $this->content;
    }

    public function set_checked($checked){
        $this->checked = $checked;
        return $this;
    }

    public function get_checked(){
        return $this->checked;
    }


}