<?php
class Souscategories extends EntityModel{

    const table = "souscategories";
    
    protected $id;
    protected $name;
    protected $id_categories;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'name' => 'name',
           'id_categories' => 'id_categories'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_name($name){
        $this->name = $name;
        return $this;
    }

    public function get_name(){
        return $this->name;
    }

    public function set_id_categories($id_categories){
        $this->id_categories = $id_categories;
        return $this;
    }

    public function get_id_categories(){
        return $this->id_categories;
    }


}