<?php
class Departement extends EntityModel{

    const table = "departement";
    
    protected $id;
    protected $nom;
    protected $id_region;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'nom' => 'nom',
           'id_region' => 'id_region'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_nom($nom){
        $this->nom = $nom;
        return $this;
    }

    public function get_nom(){
        return $this->nom;
    }

    public function set_id_region($id_region){
        $this->id_region = $id_region;
        return $this;
    }

    public function get_id_region(){
        return $this->id_region;
    }


}