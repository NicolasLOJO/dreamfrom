<?php
class Adresse extends EntityModel{

    const table = "adresse";
    
    protected $id;
    protected $numero;
    protected $voie;
    protected $nom;
    protected $id_ville;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'numero' => 'numero',
           'voie' => 'voie',
           'nom' => 'nom',
           'id_ville' => 'id_ville'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_numero($numero){
        $this->numero = $numero;
        return $this;
    }

    public function get_numero(){
        return $this->numero;
    }

    public function set_voie($voie){
        $this->voie = $voie;
        return $this;
    }

    public function get_voie(){
        return $this->voie;
    }

    public function set_nom($nom){
        $this->nom = $nom;
        return $this;
    }

    public function get_nom(){
        return $this->nom;
    }

    public function set_id_ville($id_ville){
        $this->id_ville = $id_ville;
        return $this;
    }

    public function get_id_ville(){
        return $this->id_ville;
    }


}