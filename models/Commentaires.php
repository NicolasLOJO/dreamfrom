<?php
class Commentaires extends EntityModel{

    const table = "commentaires";
    
    protected $id;
    protected $content;
    protected $created_date;
    protected $id_user;
    protected $id_bar;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'content' => 'content',
           'created_date' => 'created_date',
           'id_user' => 'id_user',
           'id_bar' => 'id_bar'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_content($content){
        $this->content = $content;
        return $this;
    }

    public function get_content(){
        return $this->content;
    }

    public function set_created_date($created_date){
        $this->created_date = $created_date;
        return $this;
    }

    public function get_created_date(){
        return $this->created_date;
    }

    public function set_id_user($id_user){
        $this->id_user = $id_user;
        return $this;
    }

    public function get_id_user(){
        return $this->id_user;
    }

    public function set_id_bar($id_bar){
        $this->id_bar = $id_bar;
        return $this;
    }

    public function get_id_bar(){
        return $this->id_bar;
    }


}