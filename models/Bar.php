<?php
class Bar extends EntityModel{

    const table = "bar";
    
    protected $id;
    protected $titre;
    protected $content;
    protected $created_date;
    protected $id_user;
    protected $id_categorie;
    protected $id_souscategorie;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'titre' => 'titre',
           'content' => 'content',
           'created_date' => 'created_date',
           'id_user' => 'id_user',
           'id_categorie' => 'id_categorie',
           'id_souscategorie' => 'id_souscategorie'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_titre($titre){
        $this->titre = $titre;
        return $this;
    }

    public function get_titre(){
        return $this->titre;
    }

    public function set_content($content){
        $this->content = $content;
        return $this;
    }

    public function get_content(){
        return $this->content;
    }

    public function set_created_date($created_date){
        $this->created_date = $created_date;
        return $this;
    }

    public function get_created_date(){
        return $this->created_date;
    }

    public function set_id_user($id_user){
        $this->id_user = $id_user;
        return $this;
    }

    public function get_id_user(){
        return $this->id_user;
    }

    public function set_id_categorie($id_categorie){
        $this->id_categorie = $id_categorie;
        return $this;
    }

    public function get_id_categorie(){
        return $this->id_categorie;
    }

    public function set_id_souscategorie($id_souscategorie){
        $this->id_souscategorie = $id_souscategorie;
        return $this;
    }

    public function get_id_souscategorie(){
        return $this->id_souscategorie;
    }


}