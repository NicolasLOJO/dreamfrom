<?php
class Privatemsg extends EntityModel{

    const table = "privatemsg";
    
    protected $id;
    protected $object;
    protected $content;
    protected $created_date;
    protected $id_user;
    protected $destinataire;
    protected $deleted_user;
    protected $deleted_destinataire;
    protected $readed_message;
    protected $id_project;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'object' => 'object',
           'content' => 'content',
           'created_date' => 'created_date',
           'id_user' => 'id_user',
           'destinataire' => 'destinataire',
           'deleted_user' => 'deleted_user',
           'deleted_destinataire' => 'deleted_destinataire',
           'readed_message' => 'readed_message',
           'id_project' => 'id_project'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_object($object){
        $this->object = $object;
        return $this;
    }

    public function get_object(){
        return $this->object;
    }

    public function set_content($content){
        $this->content = $content;
        return $this;
    }

    public function get_content(){
        return $this->content;
    }

    public function set_created_date($created_date){
        $this->created_date = $created_date;
        return $this;
    }

    public function get_created_date(){
        return $this->created_date;
    }

    public function set_id_user($id_user){
        $this->id_user = $id_user;
        return $this;
    }

    public function get_id_user(){
        return $this->id_user;
    }

    public function set_destinataire($destinataire){
        $this->destinataire = $destinataire;
        return $this;
    }

    public function get_destinataire(){
        return $this->destinataire;
    }

    public function set_deleted_user($deleted_user){
        $this->deleted_user = $deleted_user;
        return $this;
    }

    public function get_deleted_user(){
        return $this->deleted_user;
    }

    public function set_deleted_destinataire($deleted_destinataire){
        $this->deleted_destinataire = $deleted_destinataire;
        return $this;
    }

    public function get_deleted_destinataire(){
        return $this->deleted_destinataire;
    }

    public function get_readed_message(){
        return $this->readed_message;
    }

    public function set_readed_message($read){
        $this->readed_message = $read;
        return $this;
    }

    public function set_id_project($project){
        $this->id_project = $project;
        return $this;
    }

    public function get_id_project(){
        return $this->id_project;
    }
}