<?php
class Ville extends EntityModel{

    const table = "ville";
    
    private $id;
    private $nom;
    private $code_postal;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'nom' => 'nom',
           'code_postal' => 'code_postal'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_nom($nom){
        $this->nom = $nom;
        return $this;
    }

    public function get_nom(){
        return $this->nom;
    }

    public function set_code_postal($code_postal){
        $this->code_postal = $code_postal;
        return $this;
    }

    public function get_code_postal(){
        return $this->code_postal;
    }


}