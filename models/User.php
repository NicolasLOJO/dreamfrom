<?php
class User extends EntityModel{

    const table = "user";
    
    protected $id;
    protected $username;
    protected $password;
    protected $status;
    protected $created_date;
    protected $avatar;
    protected $nom;
    protected $prenom;
    protected $id_departement;
    protected $private;
    protected $contact;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'nom' => 'nom',
           'prenom' => 'prenom',
           'username' => 'username',
           'password' => 'password',
           'status' => 'status',
           'created_date' => 'created_date',
           'avatar' => 'avatar',
           'id_departement' => 'id_departement',
            'private' => 'private',
            'contact' => 'contact'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_username($username){
        $this->username = $username;
        return $this;
    }

    public function get_username(){
        return $this->username;
    }

    public function set_password($password){
        $this->password = $password;
        return $this;
    }

    public function get_password(){
        return $this->password;
    }

    public function set_status($status){
        $this->status = $status;
        return $this;
    }

    public function get_status(){
        return $this->status;
    }

    public function set_created_date($created_date){
        $this->created_date = $created_date;
        return $this;
    }

    public function get_created_date(){
        return $this->created_date;
    }

    public function set_avatar($avatar){
        $this->avatar = $avatar;
        return $this;
    }

    public function get_avatar(){
        return $this->avatar;
    }

    public function set_id_departement($id_departement){
        $this->id_departement = $id_departement;
        return $this;
    }

    public function get_id_departement(){
        return $this->id_departement;
    }

    public function get_nom(){
        return $this->nom;
    }

    public function get_prenom(){
        return $this->prenom;
    }

    public function set_nom($nom){
        $this->nom = $nom;
        return $this;
    }

    public function set_prenom($prenom){
        $this->prenom = $prenom;
        return $this;
    }

    public function set_contact($contact){
        $this->contact = $contact;
        return $this;
    }

    public function get_contact(){
        return $this->contact;
    }

    public function set_private($private){
        $this->private = $private;
        return $this;
    }

    public function get_private(){
        return $this->private;
    }
}