<?php
class Images extends EntityModel{

    const table = "images";
    
    private $id;
    private $path;
    private $nom;
    private $id_project;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'path' => 'path',
           'nom' => 'nom',
           'id_project' => 'id_project'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_path($path){
        $this->path = $path;
        return $this;
    }

    public function get_path(){
        return $this->path;
    }

    public function set_nom($nom){
        $this->nom = $nom;
        return $this;
    }

    public function get_nom(){
        return $this->nom;
    }

    public function set_id_project($id_project){
        $this->id_project = $id_project;
        return $this;
    }

    public function get_id_project(){
        return $this->id_project;
    }


}