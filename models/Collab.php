<?php
class Collab extends EntityModel{

    const table = "collab";
    
    protected $id;
    protected $id_user;
    protected $id_project;
    protected $added_date;


    public function __construct(){
        parent::__construct();
        $array = [
            'id' => 'id',
           'id_user' => 'id_user',
           'id_project' => 'id_project',
            'added_date' => 'added_date'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }
    
    public function set_added_date($date){
        $this->added_date = $date;
        return $this;
    }

    public function get_added_date(){
        return $this->added_date;
    }

    public function set_id_user($id_user){
        $this->id_user = $id_user;
        return $this;
    }

    public function get_id_user(){
        return $this->id_user;
    }

    public function set_id_project($id_project){
        $this->id_project = $id_project;
        return $this;
    }

    public function get_id_project(){
        return $this->id_project;
    }


}