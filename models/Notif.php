<?php
class Notif extends EntityModel{

    const table = "notif";
    
    protected $id;
    protected $id_user;
    protected $id_project;
    protected $accepted;
    protected $expediteur;
    protected $lien;
    protected $hidden;
    protected $type;
    protected $status;


    public function __construct(){
        parent::__construct();
        $array = [
           'id' => 'id',
           'id_user' => 'id_user',
           'id_project' => 'id_project',
           'accepted' => 'accepted',
           'expediteur' => 'expediteur',
           'lien' => 'lien',
           'hidden' => 'hidden',
           'type' => 'type',
            'status' => 'status'];
    }

    public function hydrate($array){
        foreach($array as $key => $value){
            $setter = "set_$key";
            $this->$setter($value);
        }
        return $this;
    }

    public function set_status($status){
        $this->status = $status;
        return $this;
    }

    public function get_status(){
        return $this->status;
    }

    public function set_id($id){
        $this->id = $id;
        return $this;
    }

    public function get_id(){
        return $this->id;
    }

    public function set_id_user($id_user){
        $this->id_user = $id_user;
        return $this;
    }

    public function get_id_user(){
        return $this->id_user;
    }

    public function set_id_project($id_project){
        $this->id_project = $id_project;
        return $this;
    }

    public function get_id_project(){
        return $this->id_project;
    }

    public function set_accepted($accepted){
        $this->accepted = $accepted;
        return $this;
    }

    public function get_accepted(){
        return $this->accepted;
    }

    public function set_expediteur($expediteur){
        $this->expediteur = $expediteur;
        return $this;
    }

    public function get_expediteur(){
        return $this->expediteur;
    }

    public function set_lien($lien){
        $this->lien = $lien;
        return $this;
    }

    public function get_lien(){
        return $this->lien;
    }

    public function set_hidden($hidden){
        $this->hidden = $hidden;
        return $this;
    }

    public function get_hidden(){
        return $this->hidden;
    }

    public function set_type($type){
        $this->type = $type;
        return $this;
    }

    public function get_type(){
        return $this->type;
    }


}