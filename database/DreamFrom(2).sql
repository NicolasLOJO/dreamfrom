-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 29, 2018 at 03:17 PM
-- Server version: 5.7.22-0ubuntu18.04.1
-- PHP Version: 7.2.5-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `DreamFrom`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`id`, `name`) VALUES
(1, 'Informatique'),
(2, 'Electronique'),
(3, 'Domotique');

-- --------------------------------------------------------

--
-- Table structure for table `collab`
--

CREATE TABLE `collab` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `added_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `collab`
--

INSERT INTO `collab` (`id`, `id_user`, `id_project`, `added_date`) VALUES
(1, 15, 1, '2018-06-26 10:42:30'),
(14, 16, 2, '2018-06-26 15:37:25'),
(15, 17, 1, '2018-06-29 12:07:11'),
(16, 15, 3, '2018-06-29 13:41:49');

-- --------------------------------------------------------

--
-- Table structure for table `commentaires`
--

CREATE TABLE `commentaires` (
  `id` int(11) NOT NULL,
  `content` text,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_user` int(11) DEFAULT NULL,
  `id_categorie` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `departement`
--

CREATE TABLE `departement` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `id_region` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departement`
--

INSERT INTO `departement` (`id`, `nom`, `id_region`) VALUES
(1, 'HERAULT', 1),
(2, 'AUDE', 1);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `id_project` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notif`
--

CREATE TABLE `notif` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_project` int(11) NOT NULL,
  `accepted` tinyint(1) DEFAULT NULL,
  `expediteur` int(11) NOT NULL,
  `lien` varchar(255) NOT NULL,
  `hidden` int(11) NOT NULL DEFAULT '0',
  `type` varchar(45) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notif`
--

INSERT INTO `notif` (`id`, `id_user`, `id_project`, `accepted`, `expediteur`, `lien`, `hidden`, `type`, `status`) VALUES
(8, 16, 2, 1, 14, '/collab/', 1, 'collab', ''),
(9, 14, 4, 1, 16, '/collab/', 1, 'collab', ''),
(20, 16, 3, NULL, 14, '/collab/', 0, 'collab', ''),
(21, 16, 3, NULL, 14, '/collab/', 0, 'collab', ''),
(22, 16, 3, NULL, 14, '/collab/', 0, 'collab', ''),
(23, 15, 3, 1, 14, '/collab/', 1, 'collab', ''),
(24, NULL, 6, 1, 14, '/Projects/', 1, 'project', 'admin'),
(25, NULL, 7, 1, 15, '/Projects/', 1, 'project', 'admin'),
(26, NULL, 8, 1, 14, '/Projects/', 1, 'project', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `privatemsg`
--

CREATE TABLE `privatemsg` (
  `id` int(11) NOT NULL,
  `object` varchar(255) NOT NULL,
  `content` text,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_user` int(11) NOT NULL,
  `destinataire` int(11) DEFAULT NULL,
  `deleted_user` tinyint(1) DEFAULT '0',
  `deleted_destinataire` tinyint(1) DEFAULT '0',
  `readed_message` tinyint(1) DEFAULT '0',
  `id_project` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `privatemsg`
--

INSERT INTO `privatemsg` (`id`, `object`, `content`, `created_date`, `id_user`, `destinataire`, `deleted_user`, `deleted_destinataire`, `readed_message`, `id_project`) VALUES
(1, 'Travail', 'Coucou niko ça va?', '2018-06-22 16:09:50', 15, 14, 0, 0, 1, 3),
(3, 'Travail', 'Oui ça va et toi?', '2018-06-25 11:27:55', 14, 15, 0, 0, 1, 3),
(4, 'Travail', 'Tu en est ou?', '2018-06-25 11:28:28', 15, 14, 0, 0, 1, 7),
(14, 'fdshfhs', 'jdfgfqgshdf', '2018-06-28 16:17:31', 17, 14, 0, 0, 1, 7),
(15, 'fdshfhs', 'coucou cacaman', '2018-06-29 09:44:22', 14, 17, 0, 0, 1, 7),
(16, 'Perdu', 'je suis perdu dans le projet aide moi!!', '2018-06-29 13:41:15', 14, 15, 0, 0, 1, 1),
(17, 'Perdu', 'je peut pas t\'aider', '2018-06-29 13:42:47', 15, 14, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_user` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `id_souscategorie` int(11) NOT NULL,
  `accepted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `titre`, `content`, `created_date`, `id_user`, `id_categorie`, `id_souscategorie`, `accepted`) VALUES
(1, 'My first project', 'this i smy first project', '2018-06-20 14:34:38', 14, 2, 1, 1),
(2, 'Test Projects', 'This is my test projects', '2018-06-26 09:48:41', 0, 2, 3, 1),
(3, 'Test notif', '  modifier hello comment va tu?', '2018-06-26 14:45:42', 0, 2, 1, 1),
(4, 'Test de projet', 'fldhdgslbgs sdlhblgsdlh lhdlksgdh lsdkghs ldhlhkdskgkl sd gsghs gk jsgsdj gsdh sdjghsdkg', '2018-06-26 15:53:08', 16, 2, 2, NULL),
(6, 'Test notif admin', 'test de notification administrateur pour validation nouveau projet', '2018-06-27 10:38:05', 14, 2, 2, 1),
(7, 'dxfcbvwxb', 'xbnc cvxb vx x', '2018-06-27 13:32:21', 15, 2, 2, 1),
(8, 'fdhgjch', 'gcxcjv,vch', '2018-06-28 11:00:14', 14, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `nom`) VALUES
(1, 'OCCITANIE');

-- --------------------------------------------------------

--
-- Table structure for table `souscategories`
--

CREATE TABLE `souscategories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_categories` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `souscategories`
--

INSERT INTO `souscategories` (`id`, `name`, `id_categories`) VALUES
(1, 'Arduino', 2),
(2, 'Raspberry', 2),
(3, 'PIC', 2);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) NOT NULL,
  `prenom` varchar(45) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` varchar(45) DEFAULT 'member',
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `avatar` varchar(255) DEFAULT 'default.png',
  `id_departement` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `username`, `password`, `status`, `created_date`, `avatar`, `id_departement`) VALUES
(14, 'Lopez', 'Nicolas', 'Niko', '$2y$10$iZj61F4dms2/0rBPThi1OOBiroFdxI694d6bFfe.kLnCwjjX3MPwm', 'admin', '2018-06-22 11:12:59', '14.avatar-user-hacker-3830b32ad9e0802c-512x512.png', 1),
(15, 'test', 'test', 'test', '$2y$10$xBb2lwBYMYgLgAi90z64v.knVSIdocnMNpVFGRta3yUI5fc6r6QyO', 'member', '2018-06-22 11:42:20', 'default.png', 1),
(16, 'Judi', 'Hello', 'Man', '$2y$10$Uy6lD479JKDZiQpHQipuju399/903mk8MWg6pVB3mvKrWHAQt0Hsm', 'member', '2018-06-26 11:08:04', 'default.png', 1),
(17, 'New', 'System', 'cacaman', '$2y$10$OC7Kny23A94Kq9fUVenFyeLhkK0U1IUQYJDfQPyL1gar4DB.1/jJi', 'member', '2018-06-28 13:48:46', 'default.png', 1),
(18, 'Admin', 'Admin', 'root', '$2y$10$pmZVKKfTJphw/duL.Tn9ze8QllHwLWLkR9auaFTm8dhPQ4/xIupDG', 'admin', '2018-06-29 15:16:56', 'default.png', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collab`
--
ALTER TABLE `collab`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_commentaires_user1_idx` (`id_user`),
  ADD KEY `fk_commentaires_categorie1_idx` (`id_categorie`);

--
-- Indexes for table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_region` (`id_region`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_images_project1_idx` (`id_project`);

--
-- Indexes for table `notif`
--
ALTER TABLE `notif`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `privatemsg`
--
ALTER TABLE `privatemsg`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_privatemsg_User_idx` (`id_user`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_project_user1_idx` (`id_user`),
  ADD KEY `fk_project_categorie1_idx` (`id_categorie`),
  ADD KEY `id_souscategorie` (`id_souscategorie`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `souscategories`
--
ALTER TABLE `souscategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categories` (`id_categories`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_adresse1_idx` (`id_departement`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `collab`
--
ALTER TABLE `collab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `departement`
--
ALTER TABLE `departement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notif`
--
ALTER TABLE `notif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `privatemsg`
--
ALTER TABLE `privatemsg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `souscategories`
--
ALTER TABLE `souscategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `collab`
--
ALTER TABLE `collab`
  ADD CONSTRAINT `collab_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `collab_ibfk_2` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`);

--
-- Constraints for table `commentaires`
--
ALTER TABLE `commentaires`
  ADD CONSTRAINT `fk_commentaires_categorie1` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_commentaires_user1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `departement`
--
ALTER TABLE `departement`
  ADD CONSTRAINT `departement_ibfk_1` FOREIGN KEY (`id_region`) REFERENCES `region` (`id`);

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `fk_images_project1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `notif`
--
ALTER TABLE `notif`
  ADD CONSTRAINT `notif_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `notif_ibfk_2` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`);

--
-- Constraints for table `privatemsg`
--
ALTER TABLE `privatemsg`
  ADD CONSTRAINT `fk_privatemsg_User` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `privatemsg_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`);

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `fk_project_categorie1` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_project_user1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`id_souscategorie`) REFERENCES `souscategories` (`id`);

--
-- Constraints for table `souscategories`
--
ALTER TABLE `souscategories`
  ADD CONSTRAINT `souscategories_ibfk_1` FOREIGN KEY (`id_categories`) REFERENCES `categorie` (`id`);

--
-- Constraints for table `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `task_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `task_ibfk_2` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_departement`) REFERENCES `departement` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
