<?php
class DAOProject extends DAO{
    
    public function retrieve($id){
        //retourne entité
        $qb = new Query();
        $sql = $qb->table("project")->select("*")->where("id = $id")->getQuery();
        $value = $qb->getValue();
        $db = $this->pdo;
        $rep = $db->prepare($sql);
        $rep->execute($value);
        return $rep->fetch(PDO::FETCH_ASSOC);
    }

    public function update($entity, $id){
        $qb = new Query();
        $sql = $qb->table("project")->update($entity)->where("id = $id")->getQuery();
        $value = $qb->getValue();
        $db = $this->pdo;
        $req = $db->prepare($sql);
        return $req->execute($value);
    }

    public function delete($id){
        $qb = new Query();
        $sql = $qb->table("project")->delete()->where("id = $id")->getQuery();
        $value = $qb->getValue();
        $db = $this->pdo;
        $req = $db->prepare($sql);
        return $req->execute($value);
        //retourne boolean
    }

    public function create($array_assoc){
        //retourne entité
        $qb = new Query();
        $sql = $qb->table("project")->insert($array_assoc)->getQuery();
        $value = $qb->getValue();
        $db = $this->pdo;
        $req = $db->prepare($sql);
        $req->execute($value);
        return $this->retrieve("LAST_INSERT_ID()");
    }

    public function getAllBy($array_assoc){
        //clause where et and dans array_assoc
        $qb = new Query();
        $sql = $qb->table("project")->select('*')->where($array_assoc)->getQuery();
        $value = $qb->getValue();
        $db = $this->pdo;
        $req = $db->prepare($sql);
        $req->execute($value);
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAll(){
        $qb = new Query();
        $sql = $qb->table("project")->select('*')->getQuery();
        $db = $this->pdo;
        $rep = $db->query($sql);
        return $rep->fetchAll(PDO::FETCH_ASSOC);
    }

    public function customData($sql, $value = null){
        $db = $this->pdo;
        if($value){
            $rep = $db->prepare($sql);
            $rep->execute($value);
        } else {
            $rep = $db->query($sql);
        }
        return $rep->fetchAll(PDO::FETCH_ASSOC);
    }

    public function customExec($sql){
        $db = $this->pdo;
        $db->query($sql);
    }

    public function disableKey(){
        $db = $this->pdo;
        $db->exec('SET FOREIGN_KEY_CHECKS = 0');
    }

    public function enableKey(){
        $db = $this->pdo;
        $db->exec('SET FOREIGN_KEY_CHECKS = 1');
    }
}