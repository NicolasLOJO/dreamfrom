<?php
class DAOPrivatemsg extends DAO{
    
    public function retrieve($id){
        //retourne entité
        $qb = new Query();
        $sql = $qb->table("privatemsg")->select("*")->where("id = $id")->getQuery();
        $value = $qb->getValue();
        $db = $this->pdo;
        $rep = $db->prepare($sql);
        $rep->execute($value);
        return $rep->fetch(PDO::FETCH_ASSOC);
    }

    public function update($entity, $id){
        $qb = new Query();
        $sql = $qb->table("privatemsg")->update($entity)->where("id = $id")->getQuery();
        $value = $qb->getValue();
        $db = $this->pdo;
        $req = $db->prepare($sql);
        return $req->execute($value);
    }

    public function delete($id){
        $qb = new Query();
        $sql = $qb->table("privatemsg")->delete()->where("id = $id")->getQuery();
        $value = $qb->getValue();
        $db = $this->pdo;
        $req = $db->prepare($sql);
        return $req->execute($value);
        //retourne boolean
    }

    public function create($array_assoc){
        //retourne entité
        $qb = new Query();
        $sql = $qb->table("privatemsg")->insert($array_assoc)->getQuery();
        $value = $qb->getValue();
        $db = $this->pdo;
        $req = $db->prepare($sql);
        $req->execute($value);
        return $this->retrieve("LAST_INSERT_ID()");
    }

    public function getAllBy($array_assoc, $selection = null, $join = null, $order = null){
        //clause where et and dans array_assoc
        $qb = new Query();
        $qb->table("privatemsg");
        if($selection){
            $qb->select($selection);
        } else {
            $qb->select('*');
        }
        if($join){
            $qb->join($join);
        }
        $qb->where($array_assoc);
        if($order){
            $qb->order($order);
        }
        $sql = $qb->getQuery();
        $value = $qb->getValue();
        $db = $this->pdo;
        $req = $db->prepare($sql);
        $req->execute($value);
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAll($array = null){
        $qb = new Query();
        $qb->table("privatemsg")->select('*');
        if($array){
            $qb->join($array);
        }
        $sql = $qb->getQuery();
        $db = $this->pdo;
        $rep = $db->query($sql);
        return $rep->fetchAll(PDO::FETCH_ASSOC);
    }

    public function customData($sql){
        $db = $this->pdo;
        $rep = $db->query($sql);
        return $rep->fetch(PDO::FETCH_ASSOC);
    }
}