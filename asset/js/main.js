function require(script) {
    $.ajax({
        url: "/asset/js/"+script,
        dataType: "script",
        success: function () {
            
        },
        error: function () {
            throw new Error("Could not load script " + script);
        }
    });
}

require("function.js");
require("test.js");

$(document).ready(function(){

    $("#title_edit_project").keypress(function(e) {
        if(e.which == 13) {
            updateProject();
        }
    });
    
    $("#valid_update_project").on("click", function(){
        updateProject();
    });
    
    $("#valid_desc_project").on("click", function() {
        updateProject();
    });
    
    $('#edit_addcollab').on("click", function() {
        $('#show-addcollab').slideToggle();
    });
    
    $("#name_addcollab").on("keyup", function(){
        var name = $(this).val();
        if(user_exist(name) == true){
            if(is_collab(name) != true){
                $(this).removeClass("border-danger").addClass("border-success");
                $("#valid_addcollab").slideDown("fast");
            } else {
                $(this).removeClass("border-success").addClass("border-danger");
                $("#valid_addcollab").slideUp("fast");
            }
        } else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#valid_addcollab").slideUp("fast");
        }
    });
    
    $('#valid_addcollab').on("click", function(){
        addCollab();
    });

    $("#register-form").fadeOut(100);
    $("#register-form-tab").click( function(){
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $("#login-div").removeClass("bg-primary rounded-left");
        $("#login-form-tab").removeClass("text-white-50");
        $("#register-div").addClass("bg-primary rounded-right");
        $(this).addClass("text-white-50");
    });

    $("#login-form-tab").click( function(){
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $("#register-div").removeClass("bg-primary rounded-right");
        $("#register-form-tab").removeClass("text-white-50");
        $("#login-div").addClass("bg-primary rounded-left");
        $(this).addClass("text-white-50");
    });

    $("#login-form").submit(function(event){
        event.preventDefault();
        $("#alert-login").text("");
        var rep = true;
        rep = emptyForm($(this));
        if(rep == false){
            $("#alert-login").append("Veuillez remplir les champs").fadeIn(200).delay(1500).fadeOut(200);
        }
        if(rep == true){
            rep = logUser();
        }
    });

    $('#register-form').submit(function(e) {
        e.preventDefault();
        var tag = $(this);
        var status = verifForm($(this));
        if(status == true){
            $("#alert-login").text("Inscription réussi").removeClass("alert-danger").addClass("alert-success").fadeIn(200).delay(1500).fadeOut(200);
            setTimeout(function(){ tag.unbind('submit').submit(); }, 1500);
        }
    });

    $("#register-region").change(function(){
        var choice = $("#register-region :selected").val();
        if(choice == 0){
            $("#alert-login").text("Veuillez selectionner une region").fadeIn(200).delay(500).fadeOut(200);
        } else {
            getDepartement(choice);
        }
    });

    $("#expand-event").on("click", function(){
        $('.expand-card').animate({height : 'toggle'}, 'slow');
        if($("#event-icon").text() == "expand_more"){
            $("#event-icon").text("expand_less");
        } else {
            $("#event-icon").text("expand_more");
        }
    });

    setTimeout(function(){
            is_notif();
            getNotif();
        }, 500);
    setInterval(function(){ is_notif(); }, 5000);

    $('#home-notif').click(function(){
        $("#notif-space").toggle('blind');
        getNotif();
    });

    $(".maj-member-icon-div").click(function(){
        if($(this).parent().find(".maj-member-icon").text() == "expand_more"){
            $(this).parent().find(".maj-member-icon").text("expand_less");
        } else {
            $(this).parent().find(".maj-member-icon").text("expand_more");
        }
        $(this).parent().find(".form-member").toggle("blind");
    });

    $("#custom-cat").change(function(){
        var choice = $("#custom-cat :selected").val();
        if(choice == 0){
            $("#alert-login").text("Veuillez selectionner une region").fadeIn(200).delay(500).fadeOut(200);
        } else {
            getSub(choice);
        }
    });

    $(".modal-test").click(function(){
        var username = $(this).parent().find(".mb-2").text();
        var content = $(this).parent().find(".card-text").text();
        var id_dest = $(this).parent().find(".dest").val();
        var object = $(this).parent().find(".object").val();
        $("#username").val(username);
        $("#destinataire").val(id_dest);
        $("#content-message").text(content);
        $("#object").val(object);
        $("#MessageModal").modal('toggle');
    });

    $("#form-submit").click(function(){
        $("#form-message").submit();
    });

    $("#foot").click(function(){
        $("#test").modal("toggle");
        var table = [];
        $(".tic").click(function(){
            var tic = $(this);
            getTest(tic, table);
        });
    });
});
