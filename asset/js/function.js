function updateProject() {
    var id = $("#id_project").val();
    var title = $("#title_edit_project").val();
    var desc = $("#desc_edit_project").val();
    var host = getUrl();
    if(title != "" && desc != ""){
        $.ajax({
            url: host+"/setting/edit/"+id,
            type: "POST",
            dataType : "json",
            data: { 
                'title' : title,
                'desc' : desc
            },
            success: function(data){
                $("#color_modal_project").addClass("bg-success");
                $("#project_modal_content").append("Modifier avec succés");
                $("#success_valid_project").modal("show");
            },
            error: function(err){

            }
        });
    } else {
        $("#color_modal_project").addClass("bg-danger");
        $("#project_modal_content").append("Champs vide");
        $("#success_valid_project").modal("show");
    }
    setTimeout(function(){ location.reload(); }, 2000);
}

function addCollab(){
    var id = $("#id_project").val();
    var collab = $('#name_addcollab').val();
    var host = getUrl();
    var url = window.location.pathname;
    $.ajax({
        url: host+url+"/collab",
        type: "POST",
        dataType: "json",
        data : {
            'name' : collab
        },
        success: function(data){
            $("#color_modal_project").addClass("bg-success");
            $("#project_modal_content").append("Collaboratueur ajouté");
            $("#success_valid_project").modal("show");
            setTimeout(function(){ location.reload(); }, 2000);
        },
        error: function(err){
            console.log(err);
        }
    });
}

function user_exist(name){
    var host = getUrl();
    var result = false;
    $.ajax({
        async: false,
        url: host+"/api/user",
        success: function(data){
            for(let i = 0; i < data.length; i++){
                if(data[i].username.toLowerCase() === name.toLowerCase()){
                    result = true;
                }
            }
        }
    });
    return result;
}

function is_collab(name){
    var host = getUrl();
    var result = false;
    var id = $("#id_project").val();
    $.ajax({
        async: false,
        url: host+"/api/collab/"+id,
        success: function(data){
            for(let i = 0; i < data.length; i++){
                if(data[i].username.toLowerCase() == name.toLowerCase()){
                    result = true;
                }
            }
        }
    });
    return result;
}

function getUrl(){
    var base_url = window.location.origin;
    return base_url;
}

function getDepartement(choice){
    var host = getUrl();
    $("#register-dep").fadeIn(100);
    $("#register-dep").html("");
    $.ajax({
        url: host+"/api/departement",
        success: function(data){
            for(let i = 0; i < data.length; i++){
                if(data[i].id_region == choice){
                    $("#register-dep").append("<option value="+data[i].id+">"+data[i].nom+"</option>");
                }
            }
        }
    })
}

function verifForm(tag){
    $("#alert-login").text("");
    var res = true;
    var empty = true;
    empty = emptyForm(tag);
    var username = $("#register-username").val();
    if(user_exist(username) == true){
        $("#alert-login").append("Nom d'utilisateur non disponible<br>");
        res = false;
    }
    var choice = $("#register-region :selected").val();
    if(choice == 0){
        $("#alert-login").append("Veuillez selectionner une region<br>");
    }
    if($("#register_password").val() != $("#register_password_confirm").val()){
        $("#alert-login").append("Mot de passe non identique<br>");
        res = false;
    }
    if($("#register-region").val() == 0) {
        res = false;
    }
    if(empty == false){
        $("#alert-login").append("Veuillez remplir les champs");
    }
    if(empty == false || res == false){
        res = false;
        $("#alert-login").fadeIn(200).delay(1500).fadeOut(200);
    }
    return res;
}

function emptyForm(tag){
    var rep = true;
    $("input[type='text'],input[type='password']",tag).each(function() {
        if($(this).val().trim() == ""){
            rep = false;
        }
    });
    return rep;
}

function logUser(){
    var host = getUrl();
    var username = $("#login_username").val();
    var password = $("#login_password").val();
    $.ajax({
        url: host+"/login",
        type: "POST",
        dataType: "json",
        data : {
            "username" : username,
            "userpass" : password
        },
        success: function(data){
            if(data.response == 200){
                $("#alert-login").text("Connexion réussi").removeClass("alert-danger").addClass("alert-success").fadeIn(200).delay(1500).fadeOut(200);
                setTimeout(function(){ window.location = host; }, 1500);
            } else {
                $("#alert-login").text("Mauvais identifiant ou mot de passe").fadeIn(200).delay(1500).fadeOut(200);
            }
        }
    });
}

function getNotif(){
    var host = getUrl();
    $("#notif-space").text("");
    $.ajax({
        url: host+'/api/notif',
        success: function(data){
            if(data.length != 0){
                for(let i = 0; i < data.length; i++){
                    if(data[i].type == 'project'){
                        $("#icon-notif").text("notifications_active");
                        $('#notif-space').append("<?php if($_SESSION['status'] == 'admin'){ ?><div class='border-bottom mt-3'><p>Nouveau projet</p><p>Titre: "+data[i].titre+"</p><p>Auteur: "+data[i].username+"</p><a class=mr-3 href=/Projects/"+data[i].id_project+">Voir le projet</a><a class=mr-3 href=/accept"+data[i].lien+data[i].id+">Accepter</a><a href=/decline"+data[i].lien+data[i].id+">Accepter</a></div> <?php } ?>");
                    }
                    else if(data[i].type = "collab"){
                        $("#icon-notif").text("notifications_active");
                        $('#notif-space').append("<div class='border-bottom mt-3'><p>Nouvelle collaboration</p><p>Titre: "+data[i].titre+"</p><p>Auteur: "+data[i].username+"</p><a class=mr-3 href=/Projects/"+data[i].id_project+">Voir le projet</a><a class=mr-3 href=/accept"+data[i].lien+data[i].id+">Accepter</a><a href=/decline"+data[i].lien+data[i].id+">Accepter</a></div>");
                    }
                }
            }
        }
    });
}

function is_notif(){
    var host = getUrl();
    $.ajax({
        url: host+'/api/notif',
        success: function(data){
            if(data.length != 0){
                $("#home-notif").effect("bounce",'slow');
            }
        }
    });
}

function getSub(choice){
    var host = getUrl();
    $("#custom-sub").text("");
    $.ajax({
        url: host+"/api/cat",
        method : "post",
        dataType: "json",
        data : {
            'id' : choice
        },
        success: function(data){
            $("#custom-sub").fadeIn();
            if(data.length == 0){
                $("#custom-sub").append("<option value=0>Pas de sous catégorie</option>")
            } else {
                for(let i = 0; i < data.length; i++){
                    $("#custom-sub").append("<option value="+data[i].id+">"+data[i].name+"</option>");
                }
            }
        }
    })
}
